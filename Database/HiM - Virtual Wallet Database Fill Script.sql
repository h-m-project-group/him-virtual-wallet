create table cards
(
	card_id int auto_increment
		primary key,
	card_type varchar(10) not null,
	card_number int(16) not null,
	expiration_date date not null,
	card_holder varchar(30) not null,
	check_number int(3) not null,
	constraint cards_card_number_uindex
		unique (card_number)
);

create table users
(
	user_id int auto_increment
		primary key,
	username varchar(20) not null,
	password varchar(30) not null,
	email varchar(30) not null,
	phone_number int(10) not null,
	card_id int null,
	photo_url varchar(500) null,
	constraint users_email_uindex
		unique (email),
	constraint users_username_uindex
		unique (username),
	constraint users_cards_card_id_fk
		foreign key (card_id) references cards (card_id)
);

