package him.virtualwallet.services;

import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.*;
import him.virtualwallet.repositories.contracts.TransactionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static him.virtualwallet.TestHelpers.*;
import static him.virtualwallet.TestHelpers.createMockCustomer;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceImplTests {

    @Mock
    TransactionRepository mockTransactionRepository;

    @InjectMocks
    TransactionServiceImpl mockTransactionService;
    VirtualWalletServiceImpl mockWalletService;

    @Test
    public void getAll_should_callRepository() {
        // Arrange
        User mockAdmin = createMockAdmin();

        Mockito.when(mockTransactionRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        mockTransactionService.getAll(mockAdmin); // why does not require user?

        // Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_should_throwException_when_userNotAuthorized() {

        User mockUser = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockTransactionService.getAll(mockUser));
    }


    @Test
    public void getById_should_returnWallet_when_matchExists() {

        // Arrange
        Transaction mockTransaction = createMockTransaction();
        Admin mockAdmin = createMockAdmin();

        Mockito.when(mockTransactionRepository.getByID(mockTransaction.getTransactionID()))
                .thenReturn(mockTransaction);

        // Act
        Transaction result = mockTransactionService.getByID(mockTransaction.getTransactionID(), mockAdmin);


        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockTransaction.getTransactionID(), result.getTransactionID())
        );
    }


    @Test
    public void getByID_should_throwException_when_userNotAuthorized() {

        Transaction mockTransaction = createMockTransaction();

        Customer mockCustomer = createMockCustomer();

        mockCustomer.setUserID(13);

        Mockito.when(mockTransactionRepository.getByID(mockTransaction.getTransactionID()))
                .thenReturn(mockTransaction);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockTransactionService.getByID(mockTransaction.getTransactionID(), mockCustomer));
    }

    @Test
    public void filter_should_callRepository() {


        // Arrange
        User user = createMockAdmin();

        Mockito.when(mockTransactionRepository.filter(
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty()
                ))
                .thenReturn(new ArrayList<>());

        // Act
        mockTransactionService.filter(
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), user);

        // Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty());
    }


    // no such check - maybe add
//
//    @Test
//    public void filter_should_throwException_when_initiatorIsNotAnAdmin() {
//
//        User mockUser = createMockCustomer();
//
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service
//                        .filter(
//                                Optional.empty(),
//                                Optional.empty(),
//                                Optional.empty(), mockUser));
//    }



    // viewUserTransactions



    

    // create

    @Test
    public void create_should_callRepository_when_UserTransactionDoesNotExist() {

        // Arrange
        User mockAdmin = createMockAdmin();
        Transaction mockTransaction = createMockTransaction();


        Mockito.when(mockTransactionRepository.getByID(mockTransaction.getTransactionID()))
                .thenReturn(mockTransaction);

//        Mockito.when(mockTransactionRepository.getByID(mockTransaction.getTransactionID()))
//                .thenReturn(mockTransaction);

//        mockTransactionRepository.create(mockTransaction);

        // Act
        mockTransactionService.create(mockTransaction, mockAdmin);

        // Assert
        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .create(mockTransaction);
    }


    // createDeposit







    // delete

    @Test
    public void delete_should_callRepository() {

        // Arrange
        Transaction mockTransaction = createMockTransaction();
        User mockUser = createMockAdmin();

        mockTransactionService.delete(mockTransaction.getTransactionID(), mockUser);

        // Act, Assert

        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .delete(mockTransaction.getTransactionID());
    }


    @Test
    public void delete_should_throwException_when_userNotAuthorized() {

        Customer mockCustomer = createMockCustomer();

        Transaction mockTransaction = createMockTransaction();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockTransactionService.delete(mockTransaction.getTransactionID(), mockCustomer));
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockTransactionService.delete(2, mockCustomer));
    }

    @Test
    public void viewUserTransactions_should_callRepository() {

        // Arrange
        User mockUser = createMockAdmin();

        mockTransactionService.viewUserTransactions(mockUser.getUsername());

        // Act, Assert

        Mockito.verify(mockTransactionRepository, Mockito.times(1))
                .viewUserTransactions(mockUser.getUsername());


    }

    @Test
    public void createDeposit_should_updateWallet() {

        Transaction mockTransaction = createMockTransaction();

//        Mockito.when(mockTransactionRepository.create(mockTransaction))
//                .thenReturn(mockTransaction);

//        Mockito.when(mockWalletService.getByID())
//                .thenReturn();

    }


//    // Arrange
//    Transaction mockTransaction = createMockTransaction();
//    Admin mockAdmin = createMockAdmin();
//
//        Mockito.when(mockTransactionRepository.getByID(mockTransaction.getTransactionID()))
//                .thenReturn(mockTransaction);
//
//    // Act
//    Transaction result = mockTransactionService.getByID(mockTransaction.getTransactionID(), mockAdmin);
//
//
//    // Assert
//        Assertions.assertAll(
//                () -> Assertions.assertEquals(mockTransaction.getTransactionID(), result.getTransactionID())
//                );


}
