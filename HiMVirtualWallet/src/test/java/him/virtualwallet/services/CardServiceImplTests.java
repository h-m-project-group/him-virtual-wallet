package him.virtualwallet.services;

import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.Card;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.CardRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static him.virtualwallet.TestHelpers.*;

@ExtendWith(MockitoExtension.class)
public class CardServiceImplTests {

    @Mock
    CardRepository mockRepository;

    @InjectMocks
    CardServiceImpl service;

    @Test
    public void getById_should_returnCard_when_matchExist() {
        // Arrange
        Card card = createMockCard();
        Admin employee = createMockAdmin();

        Mockito.when(mockRepository.getByID(card.getCardID()))
                .thenReturn(card);
        // Act
        Card result = service.getByID(card.getCardID(), employee);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(card.getCardID(), result.getCardID()),
                () -> Assertions.assertEquals(card.getCardHolder(), result.getCardHolder())
        );
    }


    @Test
    public void getByID_should_throwException_when_userNotAuthorized() {
        Card card = createMockCard();
        Customer mockUser = createMockCustomer2();
        User admin = createMockAdmin();

        Mockito.when(mockRepository.getByID(card.getCardID()))
                .thenReturn(card);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByID(card.getCardID(), mockUser));
    }

    @Test
    public void update_should_callRepository() {
        // Arrange
        User mockAdmin = createMockAdmin();
        Card mockCard = createMockCard();

        // Act
        service.update(mockCard, mockAdmin);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCard);
    }

    @Test
    public void update_should_throwException_when_userNotAuthorized() {
        Card mockCard = createMockCard();
        User mockUser = createMockCustomer2();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCard, mockUser));

    }

    @Test
    public void delete_should_callRepository() {
        // Arrange
        Card card = createMockCard();
        User mockInitiator = createMockAdmin();

        Mockito.when(mockRepository.getByID(card.getCardID()))
                .thenReturn(card);
        service.delete(card.getCardID(), mockInitiator);

        // Act, Assert

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(card.getCardID());
    }


    @Test
    public void delete_should_throwException_when_userNotAuthorized() {
        User mockUser = createMockCustomer2();
        Card card = createMockCard();

        Mockito.when(mockRepository.getByID(card.getCardID()))
                .thenReturn(card);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(card.getCardID(), mockUser));

    }


}
