package him.virtualwallet.services;

import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VirtualWallet;
import him.virtualwallet.repositories.contracts.VirtualWalletRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static him.virtualwallet.TestHelpers.*;

@ExtendWith(MockitoExtension.class)
public class VirtualWalletServiceImplTests {


    @Mock
    VirtualWalletRepository mockWalletRepository;

    @InjectMocks
    VirtualWalletServiceImpl mockWalletService;

    @InjectMocks
    UserServiceImpl mockUserService;


    @Test
    public void getAll_should_callRepository() {
        // Arrange
        User mockAdmin = createMockAdmin();

        Mockito.when(mockWalletRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        mockWalletService.getAll(mockAdmin); // why does not require user?

        // Assert
        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAll_should_throwException_when_userNotAuthorized() {

        User mockUser = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.getAll(mockUser));
    }


    @Test
    public void getById_should_returnWallet_when_matchExists() {
        // Arrange
        VirtualWallet mockWallet = createMockWallet();
        Admin mockAdmin = createMockAdmin();

        Mockito.when(mockWalletRepository.getByID(mockWallet.getWalletID()))
                .thenReturn(mockWallet);
        // Act
        VirtualWallet result = mockWalletService.getByID(mockWallet.getWalletID(), mockAdmin);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWallet.getWalletID(), result.getWalletID())
        );
    }


    @Test
    public void getByID_should_throwException_when_userNotAuthorized() {

        VirtualWallet mockWallet = createMockWallet();

       // mockWalletRepository.create();

        Customer mockCustomer = createMockCustomer();

        mockCustomer.setUserID(13);

        Mockito.when(mockWalletRepository.getByID(mockWallet.getWalletID()))
                .thenReturn(mockWallet);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.getByID(mockWallet.getWalletID(), mockCustomer));
    }


    @Test
    public void getByUserID_should_returnWallet_when_matchExist() {
        // Arrange
        VirtualWallet mockWallet = createMockWallet();
        User mockAdmin = createMockAdmin();
        User mockCustomer = mockUserService.getByID(mockWallet.getUser().getUserID(), mockAdmin);

        Mockito.when(mockWalletRepository.getByID(mockWallet.getWalletID()))
                .thenReturn(mockWallet);
        // Act
        VirtualWallet result = mockWalletService.getByUserID(mockCustomer.getUserID());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWallet.getUser().getUserID(), result.getUser().getUserID())
        );
    }

    @Test
    public void getWalletByUserID_should_returnWallet_when_matchExist() {
        // Arrange
        Customer mockCustomer = createMockCustomer();
        VirtualWallet mockWallet = createMockWallet();
        Admin mockAdmin = createMockAdmin();

//        Mockito.when(mockWalletRepository.getByID(mockWallet.getWalletID()))
//                .thenReturn(mockWallet);
        // Act
        VirtualWallet result = mockWalletService.getByUserID(mockCustomer.getUserID());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWallet.getUser().getUserID(), result.getUser().getUserID())
        );
    }

    // NO SUCH LOGIC

    @Test
    public void update_should_throwException_when_userIsNotAdmin() {
        // Arrange
        VirtualWallet mockWallet = createMockWallet();
        User mockUser = createMockAdmin();

        mockWalletService.update(mockWallet, mockUser);

        // Act, Assert

        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .update(mockWallet);
    }

    // NO SUCH LOGIC
    @Test
    public void update_should_throwException_when_userNotAuthorized() {

        VirtualWallet mockWallet = createMockWallet();
        User mockUser = createMockCustomer();
        mockUser.setUserID(4);

        Customer mockCustomer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.update(mockWallet, mockUser));
    }



    @Test
    public void delete_should_callRepository() {

        // Arrange
        VirtualWallet mockWallet = createMockWallet();
        User mockUser = createMockAdmin();

        Mockito.when(mockWalletRepository.getByID(mockWallet.getWalletID()))
                .thenReturn(mockWallet);

        mockWalletService.delete(mockWallet.getWalletID(), mockUser);

        // Act, Assert

        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .delete(mockWallet.getWalletID());
    }



    @Test
    public void delete_should_throwException_when_userNotAuthorized() {

//        User mockUser = createMockCustomer();
        Customer mockCustomer = createMockCustomer();

        mockCustomer.setUserID(66);

        VirtualWallet mockWallet = createMockWallet();

        Mockito.when(mockWalletRepository.getByID(mockWallet.getWalletID()))
                .thenReturn(mockWallet);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.delete(mockWallet.getWalletID(), mockCustomer));
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockWalletService.delete(1, mockCustomer));
    }





}
