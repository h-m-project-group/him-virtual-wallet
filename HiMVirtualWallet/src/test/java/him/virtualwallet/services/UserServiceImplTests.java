package him.virtualwallet.services;

import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static him.virtualwallet.TestHelpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        // Arrange
        User mockEmployee = createMockAdmin();

        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());
        // Act
        service.getAll(mockEmployee);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getAllCustomers_should_throwException_when_userNotAuthorized() {

        User mockUser = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAll(mockUser));
    }

    @Test
    public void getByEmail_should_returnCustomer_when_matchExist() {
        // Arrange
        Customer customer = createMockCustomer();
        Admin employee = createMockAdmin();

        Mockito.when(mockRepository.getByEmail(customer.getEmail()))
                .thenReturn(customer);
        // Act
        User result = service.getByEmail(customer.getEmail());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(customer.getEmail(), result.getEmail())
        );
    }

    @Test
    public void getByUsername_should_returnCustomer_when_matchExist() {
        // Arrange
        Customer customer = createMockCustomer();
        Admin employee = createMockAdmin();

        Mockito.when(mockRepository.getByUserName(customer.getUsername()))
                .thenReturn(customer);
        // Act
        User result = service.getByUserName(customer.getUsername());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(customer.getUsername(), result.getUsername())
        );
    }


    @Test
    public void getById_should_returnCustomer_when_matchExist() {
        // Arrange
        Customer customer = createMockCustomer();
        Admin employee = createMockAdmin();

        Mockito.when(mockRepository.getByID(customer.getUserID()))
                .thenReturn(customer);
        // Act
        User result = service.getByID(customer.getUserID(), employee);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(customer.getUserID(), result.getUserID()),
                () -> Assertions.assertEquals(customer.getUsername(), result.getUsername())
        );
    }


    @Test
    public void getCustomerInfoByID_should_returnCustomer_when_matchExist() {
        // Arrange
        Customer customer = createMockCustomer();
        Admin employee = createMockAdmin();

        Mockito.when(mockRepository.getByID(customer.getUserID()))
                .thenReturn(customer);
        // Act
        User result = service.getCustomerInfoByID(customer.getUserID(), employee);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(customer.getUserID(), result.getUserID()),
                () -> Assertions.assertEquals(customer.getUsername(), result.getUsername())
        );
    }

    @Test
    public void getByID_should_throwException_when_userNotAuthorized() {

        Customer mockCustomer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByID(mockCustomer.getUserID(), mockCustomer));
    }

    @Test
    public void getByUserNameWithCard_should_returnCustomer_when_matchExist() {
        // Arrange
        Customer customer = createMockCustomer();
        Admin employee = createMockAdmin();

        Mockito.when(mockRepository.getByUsernameWithCard(customer.getUsername()))
                .thenReturn(customer);
        // Act
        User result = service.getByUsernameWithCard(customer.getUsername(), employee);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(customer.getUserID(), result.getUserID()),
                () -> Assertions.assertEquals(customer.getUsername(), result.getUsername())
        );
    }

    @Test
    public void getByUserNameWithCard_should_throwException_when_userNotAuthorized() {
        //Arrange
        Customer mockCustomer = createMockCustomer();

        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getByUsernameWithCard(mockCustomer.getUsername(), mockCustomer));
    }

    @Test
    public void create_should_callRepository_when_UserDoesntExist() {
        // Arrange
        User mockAdmin = createMockAdmin();
        Customer mockUser = createMockCustomer();
//        Mockito.when(mockRepository.getByUserName("username"))
//                .thenReturn(mockUser);
        // Act
        service.create(mockUser,mockAdmin);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void register_should_throw_when_UserAlreadyExists() {

        // Arrange
        Customer mockCustomer2 = createMockCustomer2();
        Customer mockCustomer = createMockCustomer();
//
//        Mockito.when(mockRepository.getByUserName(mockCustomer.getUsername()))
//                .thenThrow(DuplicateEntityException.class);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.registerCustomer(mockCustomer));
    }

    @Test
    public void delete_should_callRepository() {
        // Arrange
        Customer mockCustomer = createMockCustomer();
        User mockInitiator = createMockAdmin();

        service.delete(mockCustomer.getUserID(), mockInitiator);

        // Act, Assert

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockCustomer.getUserID());
    }

    @Test
    public void delete_should_throwException_when_userNotAuthorized() {

        User mockUser = createMockCustomer();

        Customer mockCustomer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockCustomer.getUserID(), mockCustomer));
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(2, mockCustomer));
    }


    @Test
    public void update_should_throwException_when_initiatorIsNotAnAdmin() {
        // Arrange
        Customer mockCustomer = createMockCustomer();
        User mockInitiator = createMockAdmin();

        service.update(mockCustomer, mockInitiator);

        // Act, Assert

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCustomer);
    }

    @Test
    public void update_should_throwException_when_userNotAuthorized() {

        User mockUser = createMockAdmin();
        User anotherUser = createMockCustomer();
        anotherUser.setUserID(4);

        Customer mockCustomer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCustomer, anotherUser));
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service.update(mockUser, anotherUser));
    }

    @Test
    public void search_should_callRepository() {
        // Arrange
        User mockUser = createMockAdmin();
        Mockito.when(mockRepository.search(""))
                .thenReturn(new ArrayList<>());

        // Act
        service.search("", mockUser);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .search("");
    }

    @Test
    public void search_should_throwException_when_userNotAuthorized() {

        Customer mockCustomer = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.search("",mockCustomer));
    }


    @Test
    public void filter_should_callRepository() {
        // Arrange
        User user = createMockAdmin();
        Mockito.when(mockRepository.filter(Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        service.filter(Optional.empty(), Optional.empty(), Optional.empty(), user);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void filter_should_throwException_when_initiatorIsNotAnAdmin() {

        User mockUser = createMockCustomer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service
                        .filter(
                                Optional.empty(),
                                Optional.empty(),
                                Optional.empty(), mockUser));
    }


}
