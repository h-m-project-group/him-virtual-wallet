package him.virtualwallet;

import him.virtualwallet.enums.TransactionStatusEnum;
import him.virtualwallet.enums.TransactionTypeEnum;
import him.virtualwallet.enums.UserStatusEnum;
import him.virtualwallet.enums.VerificationTypeEnum;
import him.virtualwallet.models.*;
import him.virtualwallet.repositories.contracts.UserRepository;
import org.mockito.InjectMocks;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class TestHelpers {

    @InjectMocks
    UserRepository mockRepository;

    public static Customer createMockCustomer() {

        Customer mockUser = new Customer();
        mockUser.setUserID(1);
        mockUser.setUsername("MockUser");
        mockUser.setEmail("mock@test.com");
        mockUser.setPassword("1244");
        mockUser.setPhoneNumber("0988907679");
        mockUser.setPhotoUrl("dummy");
        mockUser.setVerificationType(VerificationTypeEnum.VERIFIED);
        mockUser.setUserStatus(UserStatusEnum.ACTIVE);

        return mockUser;
    }



    public static Customer createMockCustomer2() {

        Customer mockUser = new Customer();
        mockUser.setUserID(2);
        mockUser.setUsername("MockUser2");
        mockUser.setEmail("mock@test.com");
        mockUser.setPassword("1244");
        mockUser.setPhoneNumber("0988907679");
        mockUser.setPhotoUrl("dummy");
        mockUser.setVerificationType(VerificationTypeEnum.VERIFIED);
        mockUser.setUserStatus(UserStatusEnum.ACTIVE);

        return mockUser;
    }


    public static Admin createMockAdmin(){
        Admin admin = new Admin();
        admin.setUserID(1);
        admin.setUsername("MockUser");
        admin.setEmail("mock@test.com");
        admin.setPassword("1244");
        admin.setPhoneNumber("0988907679");
        return admin;
    }



    public static Transaction createMockTransaction() {
        User mockRecipient = createMockCustomer();
        User mockSender = createMockCustomer();
        LocalDateTime now = LocalDateTime.now();
        Transaction transaction = new Transaction();
        transaction.setTransactionID(1);
        transaction.setTransactionStatus(TransactionStatusEnum.CONFIRMED);
        transaction.setTransactionType(TransactionTypeEnum.OUTGOING);
        transaction.setSender(mockSender);
        transaction.setTransferSum(100);
        transaction.setRecipient(mockRecipient);
        transaction.setDate(now);

        return transaction;
    }


    public static VirtualWallet createMockWallet() {

        var mockWallet = new VirtualWallet();
        User mockUser = createMockCustomer();
        mockWallet.setWalletID(1);
        mockWallet.setTotalAmount(10000);
        mockWallet.setUser(mockUser);
        return mockWallet;
    }


    public static Card createMockCard() {
        Card mockCard = new Card();
        User mockUser = createMockCustomer();
        mockCard.setCardID(1);
        mockCard.setCardType("debit");
        mockCard.setCardHolder("Mock User");
        mockCard.setCardNumber(1234567098);
        mockCard.setCheckNumber(432);
        mockCard.setUser(mockUser);
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date expirationDate = null;
        try {
            expirationDate = df.parse("15-08-2023");
            mockCard.setExpirationDate(expirationDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return mockCard;
    }
}












