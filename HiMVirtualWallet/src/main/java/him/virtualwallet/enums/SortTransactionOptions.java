package him.virtualwallet.enums;

import java.util.HashMap;
import java.util.Map;

public enum SortTransactionOptions {

    AMOUNT_ASC("Amount, ascending"," order by transferSum asc "),
    AMOUNT_DESC("Amount, descending"," order by transferSum desc "),
    DATE_ASC("Date, ascending"," order by date asc"),
    DATE_DESC("Date, descending"," order by date desc");

    private final String preview;

    private final String query;

    private static final Map<String, SortTransactionOptions> BY_PREVIEW = new HashMap<>();

    static {
        for(SortTransactionOptions e : values()){
            BY_PREVIEW.put(e.preview, e);
        }
    }

    SortTransactionOptions(String preview, String query) {
        this.preview = preview;
        this.query = query;
    }

    public static SortTransactionOptions valueOfPreview(String preview){
        return BY_PREVIEW.get(preview);
    }

    public String getPreview() {
        return preview;
    }

    public String getQuery() {
        return query;
    }

}
