package him.virtualwallet.enums;

public enum UserStatusEnum {
    ACTIVE,
    BLOCKED,
    ADMIN
}
