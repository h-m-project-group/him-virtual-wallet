package him.virtualwallet.enums;

public enum TransactionStatusEnum {
    REJECTED,
    PROCESSING,
    CONFIRMED
}
