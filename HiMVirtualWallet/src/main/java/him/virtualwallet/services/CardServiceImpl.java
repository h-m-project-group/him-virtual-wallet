package him.virtualwallet.services;

import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.Card;
import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.BaseGetRepository;
import him.virtualwallet.repositories.contracts.BaseModifyRepository;
import him.virtualwallet.repositories.contracts.CardRepository;
import him.virtualwallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl extends BaseModifyServiceImpl<Card> implements CardService {

    private static final String ONLY_ADMINS_OR_CARD_HOLDERS_ERROR_MESSAGE =
            "Only admins or card holders can access card's information!";
    private static final String ONLY_ADMINS_OR_CARD_HOLDERS_UPDATE_ERROR_MESSAGE =
            "Only admins or card holders can update card's information!";
    private static final String ONLY_ADMINS_OR_CARD_HOLDERS_DELETE_ERROR_MESSAGE =
            "Only admins or card holders can delete a card!";

    private final CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(
            BaseGetRepository<Card> baseGetRepository,
            BaseModifyRepository<Card> baseModifyRepository,
            CardRepository cardRepository) {
        super(Card.class, baseGetRepository, baseModifyRepository);
        this.cardRepository = cardRepository;
    }

    @Override
    public Card getByID(int id, User user) {

        Card card = cardRepository.getByID(id);

        if (card.getUser().getUserID() != user.getUserID()
                && !user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_CARD_HOLDERS_ERROR_MESSAGE);
        }

        return cardRepository.getByID(id);
    }

    @Override
    public void update(Card card, User user) {

        if (card.getUser().getUserID() != user.getUserID() && !user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_CARD_HOLDERS_UPDATE_ERROR_MESSAGE);
        }

        cardRepository.update(card);
    }

    @Override
    public void delete(int id, User user) {

        Card card = cardRepository.getByID(id);

        if (card.getUser().getUserID() != user.getUserID() && !user.getClass().getName().equals(Admin.class.getName())) {

            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_CARD_HOLDERS_DELETE_ERROR_MESSAGE);
        }

        cardRepository.delete(id);
    }

    @Override
    public List<Card> getAllCustomerCards(User user) {
        return cardRepository.getAllCustomerCards(user);
    }

    @Override
    public Card getCardByNumber(long cardNumber){
        return cardRepository.getCardByNumber(cardNumber);
    }

}
