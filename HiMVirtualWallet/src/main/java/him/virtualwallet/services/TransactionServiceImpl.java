package him.virtualwallet.services;

import him.virtualwallet.exceptions.DummyBankServerException;
import him.virtualwallet.exceptions.InsufficientBalanceException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.*;
import him.virtualwallet.repositories.contracts.BaseGetRepository;
import him.virtualwallet.repositories.contracts.BaseModifyRepository;
import him.virtualwallet.repositories.contracts.TransactionRepository;
import him.virtualwallet.repositories.contracts.VirtualWalletRepository;
import him.virtualwallet.services.contracts.DummyBankService;
import him.virtualwallet.services.contracts.TransactionService;
import him.virtualwallet.services.contracts.VirtualWalletService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImpl extends BaseModifyServiceImpl<Transaction> implements TransactionService {

    private static final String TRANSACTIONS_GET_BY_ID_ERROR_MESSAGE = "You can only see your own transactions!";
    private static final String TRANSACTIONS_DELETE_ERROR_MESSAGE = "You admins are allowed to delete transactions!";
    private static final String INSUFFICIENT_BALANCE_EXCEPTION = "You have insufficient funds. Please recharge your wallet!";
    private static final String BLOCKED_USERS_ERROR_MESSAGE = "Blocked users cannot add funds to their wallet!";
    private static final String PLEASE_TRY_AGAIN_MESSAGE = "There is a problem with the server. Please try again!";
    private static final String BLOCKED = "BLOCKED";

    private final TransactionRepository transactionRepository;
    private final VirtualWalletService walletService;
    private final DummyBankService dummyBankService;
    private final SessionFactory sessionFactory;
    private final VirtualWalletRepository virtualWalletRepository;


    @Autowired
    public TransactionServiceImpl(
            BaseGetRepository<Transaction> baseGetRepository,
            BaseModifyRepository<Transaction> baseModifyRepository,
            TransactionRepository transactionRepository, VirtualWalletService walletService, DummyBankService dummyBankService, SessionFactory sessionFactory, VirtualWalletRepository virtualWalletRepository) {

        super(Transaction.class, baseGetRepository, baseModifyRepository);
        this.transactionRepository = transactionRepository;
        this.walletService = walletService;
        this.dummyBankService = dummyBankService;
        this.sessionFactory = sessionFactory;
        this.virtualWalletRepository = virtualWalletRepository;
    }

    @Override
    public Transaction getByID(int id, User user) {

        Transaction transaction = transactionRepository.getByID(id);

        if (transaction.getRecipient().getUserID() != user.getUserID()
                && transaction.getSender().getUserID() != user.getUserID()
                && !user.getClass().getName().equals(Admin.class.getName())) {

            throw new UnauthorizedOperationException(TRANSACTIONS_GET_BY_ID_ERROR_MESSAGE);
        }
        return transactionRepository.getByID(id);
    }

    @Override
    public List<Transaction> filter(Optional<String> date,
                                    Optional<String> sender,
                                    Optional<String> recipient,
                                    Optional<String> transactionType,
                                    Optional<String> sortOption,
                                    User user) {

        return transactionRepository.filter(date, sender, recipient, transactionType, sortOption);
    }

    @Override
    public List<Transaction> getAllTransactions(User user) {

        return transactionRepository.getAll();
    }

    @Override
    public List<Transaction> viewUserTransactions(String username) {

        return transactionRepository.viewUserTransactions(username);
    }

    @Override
    public void create(Transaction transaction, User user) {
        if (user.getUserStatus().toString().equals(BLOCKED)) {
            throw new UnauthorizedOperationException(BLOCKED_USERS_ERROR_MESSAGE);
        }


        VirtualWallet withdrawWallet = walletService
                .getByUserID(transaction.getSender().getUserID());
        VirtualWallet depositWallet = walletService
                .getByUserID(transaction.getRecipient().getUserID());

        if (transaction.getTransferSum() > withdrawWallet.getTotalAmount()) {
            throw new InsufficientBalanceException(INSUFFICIENT_BALANCE_EXCEPTION);
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            transactionRepository.create(transaction);

            withdrawWallet.decreaseBalance(transaction.getTransferSum());
            virtualWalletRepository.update(withdrawWallet);

            depositWallet.increaseBalance(transaction.getTransferSum());
            virtualWalletRepository.update(depositWallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public void createDeposit(Transaction transaction, User user) {
        if (user.getUserStatus().toString().equals(BLOCKED)) {
            throw new UnauthorizedOperationException(BLOCKED_USERS_ERROR_MESSAGE);
        }
        HttpStatus dummyResponse = dummyBankService.getDummyBankResponse();

        if (dummyResponse.is5xxServerError()) {
            throw new DummyBankServerException(PLEASE_TRY_AGAIN_MESSAGE);
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            transactionRepository.create(transaction);

            VirtualWallet wallet = walletService.getByUserID(user.getUserID());
            wallet.increaseBalance(transaction.getTransferSum());

            virtualWalletRepository.update(wallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id, User user) {

        if (!user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(TRANSACTIONS_DELETE_ERROR_MESSAGE);
        }

        transactionRepository.delete(id);
    }

    public List<Transaction> getTransactionsByPage(int pageId, int total) {
        return transactionRepository.getTransactionsByPage(pageId, total);
    }

    public List<Transaction> getCustomersTransactionsByPage(int pageId, int total, String username) {
        return transactionRepository.getCustomersTransactionsByPage(pageId, total, username);
    }

}


