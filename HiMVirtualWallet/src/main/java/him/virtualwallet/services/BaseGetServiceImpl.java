package him.virtualwallet.services;

import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.BaseGetRepository;
import him.virtualwallet.services.contracts.BaseGetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public abstract class BaseGetServiceImpl<T> implements BaseGetService<T> {

    public static final String ADMINS_ONLY_ERROR_MESSAGE = "Only admin users are authorized to perform this operation!";

    private final Class<T> clazz;
    private final BaseGetRepository<T> baseGetRepository;

    @Autowired
    public BaseGetServiceImpl(Class<T> clazz, BaseGetRepository<T> baseGetRepository){
        this.clazz = clazz;
        this.baseGetRepository = baseGetRepository;
    }

    @Override
    public List<T> getAll(User user) {

        if (!user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ADMINS_ONLY_ERROR_MESSAGE);
        }
        return baseGetRepository.getAll();
    }

    @Override
    public T getByID(int id, User user) {

        if (!user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ADMINS_ONLY_ERROR_MESSAGE);
        }
      return baseGetRepository.getByID(id);
    }

}
