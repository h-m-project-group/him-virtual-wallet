package him.virtualwallet.services;

import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VirtualWallet;
import him.virtualwallet.repositories.contracts.BaseGetRepository;
import him.virtualwallet.repositories.contracts.BaseModifyRepository;
import him.virtualwallet.repositories.contracts.VirtualWalletRepository;
import him.virtualwallet.services.contracts.VirtualWalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VirtualWalletServiceImpl
        extends BaseModifyServiceImpl<VirtualWallet>
        implements VirtualWalletService {

    private static final String DELETE_WALLET_ERROR_MESSAGE = "Only admins or the wallet's owner can delete this virtual wallet.";
    private static final String ONLY_ADMINS_OR_WALLET_OWNERS_ERROR_MESSAGE =
            "Only admins or the wallet's owner can access a wallet's information!";

    private final VirtualWalletRepository virtualWalletRepository;

    @Autowired
    public VirtualWalletServiceImpl(
            BaseGetRepository<VirtualWallet> baseGetRepository,
            BaseModifyRepository<VirtualWallet> baseModifyRepository,
            VirtualWalletRepository virtualWalletRepository) {

        super(VirtualWallet.class, baseGetRepository, baseModifyRepository);
        this.virtualWalletRepository = virtualWalletRepository;
    }

    @Override
    public VirtualWallet getByID(int id, User user) {

        VirtualWallet wallet = virtualWalletRepository.getByID(id);

        if (wallet.getUser().getUserID() != user.getUserID()
                && !user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_OR_WALLET_OWNERS_ERROR_MESSAGE);
        }

        return virtualWalletRepository.getByID(id);
    }

    public VirtualWallet getByUserID(int userID){
        return virtualWalletRepository.getByUserID(userID);
    }

    @Override
    public void update(VirtualWallet virtualWallet, User user) {
        virtualWalletRepository.update(virtualWallet);
    }

    @Override
    public void delete(int id, User user) {

        VirtualWallet virtualWallet= virtualWalletRepository.getByID(id);

        if (virtualWallet.getUser().getUserID() != user.getUserID()
                && !user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(DELETE_WALLET_ERROR_MESSAGE);
        }

        virtualWalletRepository.delete(id);
    }

}
