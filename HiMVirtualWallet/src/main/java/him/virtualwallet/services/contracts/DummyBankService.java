package him.virtualwallet.services.contracts;

import org.springframework.http.HttpStatus;

public interface DummyBankService {
    HttpStatus getDummyBankResponse();
}
