package him.virtualwallet.services.contracts;

import him.virtualwallet.models.Card;
import him.virtualwallet.models.User;

import java.util.List;

public interface CardService extends BaseModifyService<Card> {

    List<Card> getAllCustomerCards(User user);
    Card getCardByNumber(long cardNumber);

}
