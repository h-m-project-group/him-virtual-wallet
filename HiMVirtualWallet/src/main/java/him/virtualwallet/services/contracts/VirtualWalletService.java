package him.virtualwallet.services.contracts;

import him.virtualwallet.models.VirtualWallet;

public interface VirtualWalletService extends BaseModifyService<VirtualWallet> {

    VirtualWallet getByUserID(int userID);

}
