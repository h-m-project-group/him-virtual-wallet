package him.virtualwallet.services.contracts;

import him.virtualwallet.models.User;

public interface BaseModifyService<T> extends  BaseGetService<T>{
    void create(T t, User user);
    void update(T t, User user);
    void delete(int id, User user);
}
