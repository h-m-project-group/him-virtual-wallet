package him.virtualwallet.services.contracts;

import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VerificationToken;

import java.util.List;
import java.util.Optional;

public interface UserService extends BaseModifyService<User>{

    User getByUserName(String username);
    User getByEmail(String email);
    User getByUsernameWithCard(String username, User authenticatedUser);
    User getUserInfoByID(int userID, User user);

    Customer getCustomerInfoByID(int userID, User authenticatedUser);

    void registerCustomer(User user);

    List<User> search(String searchTerm, User authenticatedUser);

    List<User> filter(Optional<String> username,
                      Optional<String> email,
                      Optional<String> phone,
                      User user);

    VerificationToken getVerificationToken(String token);

    void createVerificationToken(User user, String token);

    Customer getCustomer(String verificationToken);

    void updateUserVerification(User user);
}
