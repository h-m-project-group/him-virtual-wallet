package him.virtualwallet.services.contracts;

import him.virtualwallet.models.Transaction;
import him.virtualwallet.models.User;

import java.util.List;
import java.util.Optional;

public interface TransactionService extends BaseModifyService<Transaction>{
    List<Transaction> filter(Optional<String> date,
                             Optional<String> sender,
                             Optional<String> recipient,
                             Optional<String> transactionType,
                             Optional<String> sortOption,
                             User user);

    List<Transaction> viewUserTransactions(String username);

    void createDeposit(Transaction transaction, User user);

    List<Transaction> getTransactionsByPage(int pageId,int total);

    List<Transaction> getAllTransactions(User user);

    List<Transaction> getCustomersTransactionsByPage(int pageId,int total,String username );

}
