package him.virtualwallet.services.contracts;

import him.virtualwallet.models.User;

import java.util.List;

public interface BaseGetService <T>{
    List<T> getAll(User user);
    T getByID(int id, User user);

}
