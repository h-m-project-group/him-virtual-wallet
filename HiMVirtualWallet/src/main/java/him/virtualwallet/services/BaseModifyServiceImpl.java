package him.virtualwallet.services;

import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.BaseGetRepository;
import him.virtualwallet.repositories.contracts.BaseModifyRepository;
import him.virtualwallet.services.contracts.BaseModifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract class BaseModifyServiceImpl<T>
        extends BaseGetServiceImpl<T>
        implements BaseModifyService<T> {

    private final BaseModifyRepository<T> baseModifyRepository;

    @Autowired
    public BaseModifyServiceImpl(Class<T> clazz,
                                 BaseGetRepository<T> baseGetRepository,
                                 BaseModifyRepository<T> baseModifyRepository) {
        super(clazz, baseGetRepository);
        this.baseModifyRepository = baseModifyRepository;
    }

    @Override
    public void create(T entity, User user) {
         baseModifyRepository.create(entity);
    }

    @Override
    public void update(T entity, User user) {
        baseModifyRepository.update(entity);
    }

    @Override
    public void delete(int id, User user) {
        baseModifyRepository.delete(id);
    }
}
