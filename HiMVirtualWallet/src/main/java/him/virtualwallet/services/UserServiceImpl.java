package him.virtualwallet.services;

import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.*;
import him.virtualwallet.repositories.contracts.*;
import him.virtualwallet.services.contracts.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl extends BaseModifyServiceImpl<User> implements UserService {

    public static final String ONLY_ADMINS_ERROR_MESSAGE = "Only admins can perform this operation";
    public static final String OPERATION_NOT_ALLOWED = "Operation not allowed!";
    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final VirtualWalletRepository walletRepository;
    private final SessionFactory sessionFactory;

    @Autowired
    public UserServiceImpl(BaseGetRepository<User> baseGetRepository,
                           BaseModifyRepository<User> baseModifyRepository,
                           UserRepository userRepository,
                           VerificationTokenRepository verificationTokenRepository,
                           VirtualWalletRepository walletRepository, SessionFactory sessionFactory) {
        super(User.class, baseGetRepository, baseModifyRepository);
        this.userRepository = userRepository;
        this.verificationTokenRepository = verificationTokenRepository;
        this.walletRepository = walletRepository;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> filter(Optional<String> username,
                             Optional<String> email,
                             Optional<String> phone,
                             User user) {
        if (!user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_ERROR_MESSAGE);
        }

        return userRepository.filter(username, email, phone);
    }

    @Override
    public void registerCustomer(User user) {

        boolean duplicateExists = true;
        try {
            userRepository.getByUserName(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", String.valueOf(user.getEmail()));
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            userRepository.create(user);
            createWallet(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<User> search(String searchTerm, User user) {
        if (!user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_ERROR_MESSAGE);
        }

        return userRepository.search(searchTerm);
    }

    @Override
    public User getByUserName(String username) {

        return userRepository.getByUserName(username);
    }

    @Override
    public User getByEmail(String email) {

        return userRepository.getByEmail(email);
    }

    @Override
    public User getByUsernameWithCard(String username, User user) {
        if (!user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ONLY_ADMINS_ERROR_MESSAGE);
        }

        return userRepository.getByUsernameWithCard(username);
    }

    @Override
    public void update(User user, User authenticated) {
        if (user.getUserID() != authenticated.getUserID()
                && !authenticated.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(OPERATION_NOT_ALLOWED);
        }

        userRepository.update(user);
    }

    @Override
    public void updateUserVerification(User user) {
        userRepository.update(user);
    }

    @Override
    public void delete(int id, User user) {
        if (user.getUserID() != user.getUserID()
                || !user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(OPERATION_NOT_ALLOWED);
        }

        userRepository.delete(id);
    }

    @Override
    public Customer getCustomerInfoByID(int userID, User authenticatedUser) {
        return userRepository.getCustomerByID(userID);
    }

    @Override
    public User getUserInfoByID(int userID, User user) {
        return userRepository.getUserByID(userID);
    }

    @Override
    public User getByID(int id, User user) {

        if (user.getUserID() != id && !user.getClass().getName().equals(Admin.class.getName())) {
            throw new UnauthorizedOperationException(ADMINS_ONLY_ERROR_MESSAGE);
        }

        return userRepository.getByID(id);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        return verificationTokenRepository.getByToken(token);
    }

    @Override
    public void createVerificationToken(User user, String token) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            Date date = Date.from(Instant.now());
            VerificationToken verificationToken = new VerificationToken();
            verificationToken.setToken(token);
            verificationToken.setUser(user);
            verificationToken.setExpiryDate(date);

            verificationTokenRepository.create(verificationToken);
            session.getTransaction().commit();
        }
    }

    @Override
    public Customer getCustomer(String verificationToken) {
        return userRepository.getCustomer(verificationToken);
    }


    private void createWallet(User user) {
        VirtualWallet wallet = new VirtualWallet();
        wallet.setUser(user);
        walletRepository.create(wallet);
    }

}
