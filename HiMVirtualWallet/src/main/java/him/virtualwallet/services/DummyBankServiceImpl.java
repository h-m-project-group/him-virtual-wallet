package him.virtualwallet.services;

import him.virtualwallet.services.contracts.DummyBankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DummyBankServiceImpl implements DummyBankService {

    private static final String DUMMY_BANK_REST_API = "http://localhost:8080/api/dummy-bank";
    private final RestTemplate restTemplate;

    @Autowired
    public DummyBankServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public HttpStatus getDummyBankResponse(){
        return restTemplate.getForObject(DUMMY_BANK_REST_API,HttpStatus.class);
    }
}
