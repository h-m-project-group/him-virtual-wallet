package him.virtualwallet.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id")
    private int cardID;

    @Column(name = "card_type")
    private String cardType;

    @Column(name = "card_number")
    private long cardNumber;

    @Column(name = "expiration_date")
    private Date expirationDate;

    @Column(name = "card_holder")
    private String cardHolder;

    @Column(name = "check_number")
    private int checkNumber;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Card() {
    }

    public Card(int cardID, String cardType, long cardNumber, Date expirationDate, String cardHolder, int checkNumber,User user) {
        this.cardID = cardID;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cardHolder = cardHolder;
        this.checkNumber = checkNumber;
        this.user = user;
    }

    // Getters

    public int getCardID() {
        return cardID;
    }

    public String getCardType() {
        return cardType;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public int getCheckNumber() {
        return checkNumber;
    }

    public User getUser() {
        return user;
    }

    // Setters

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public void setCheckNumber(int checkNumber) {
        this.checkNumber = checkNumber;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
