package him.virtualwallet.models;

import him.virtualwallet.enums.UserStatusEnum;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("admin")
public class Admin extends User{

    public Admin(int userID,
                      String username,
                      String password,
                      String email,
                      String phoneNumber,
                        String photoUrl,
                      UserStatusEnum userStatus) {
        super(userID, username, password, email, phoneNumber, photoUrl, userStatus);
    }

    public Admin() {

    }
}
