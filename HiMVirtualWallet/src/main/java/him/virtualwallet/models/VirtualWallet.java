package him.virtualwallet.models;

import javax.persistence.*;

@Entity
@Table(name = "wallets")
public class VirtualWallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wallet_id")
    private int walletID;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "total_amount")
    private double totalAmount;

    public VirtualWallet(){

    }

    public VirtualWallet(int walletID, User user, double totalAmount) {
        this.walletID = walletID;
        this.user = user;
        this.totalAmount = totalAmount;
    }

    public int getWalletID() {
        return walletID;
    }

    public void setWalletID(int walletID) {
        this.walletID = walletID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    // Methods

    public void increaseBalance(double amount){
        setTotalAmount(getTotalAmount() + amount);
    }

    public void decreaseBalance(double amount){
        setTotalAmount(getTotalAmount() - amount);
    }
}
