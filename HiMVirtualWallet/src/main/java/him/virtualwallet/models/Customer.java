package him.virtualwallet.models;

import him.virtualwallet.enums.UserStatusEnum;
import him.virtualwallet.enums.VerificationTypeEnum;

import javax.persistence.*;

@Entity
@DiscriminatorValue("customer")
public class Customer extends User{

    @Enumerated(EnumType.STRING)
    @Column(name = "verification_type")
    private VerificationTypeEnum verificationType;


    public Customer(int userID,
                    String username,
                    String password,
                    String email,
                    String phoneNumber,
                    String photoUrl,
                    UserStatusEnum userStatus,
                    VerificationTypeEnum verificationType) {
        super(userID,
                username,
                password,
                email,
                phoneNumber,
                photoUrl,
                userStatus);
        this.verificationType = verificationType;
    }

    public Customer() {

    }

    public VerificationTypeEnum getVerificationType() {
        return verificationType;
    }

    public void setVerificationType(VerificationTypeEnum verificationType) {
        this.verificationType = verificationType;
    }
}
