package him.virtualwallet.models.dtos;

import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;

public class TransactionDto {

    @NotBlank(message = "Please enter a username")
    private String username;

    @NumberFormat(pattern = "#,###.##")
    @Digits(integer = 5,fraction = 2,message = "The maximum amount allowed for transfer is 10,000")
    private double transferSum;

    public TransactionDto() {
    }

    public TransactionDto(String username, double transferSum) {
        this.username = username;
        this.transferSum = transferSum;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getTransferSum() {
        return transferSum;
    }

    public void setTransferSum(double transferSum) {
        this.transferSum = transferSum;
    }

}
