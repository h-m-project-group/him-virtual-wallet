package him.virtualwallet.models.dtos;

public class VirtualWalletDto {

    private int walletID;
    private int userID;

    public VirtualWalletDto() {
    }

    public VirtualWalletDto(int walletID, int userID) {
        this.walletID = walletID;
        this.userID = userID;
    }

    // Getters

    public int getWalletID() {
        return walletID;
    }

    public int getUserID() {
        return userID;
    }

    // Setters

    public void setWalletID(int walletID) {
        this.walletID = walletID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
