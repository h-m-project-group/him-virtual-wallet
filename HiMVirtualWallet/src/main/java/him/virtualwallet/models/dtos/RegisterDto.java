package him.virtualwallet.models.dtos;

import javax.validation.constraints.*;

public class RegisterDto extends LoginDto {

    private static final String SECURE_PASSWORD_REQUIREMENTS = "Secure Password requirements\n" +
            "\n" +
            "Password must contain at least one digit [0-9].\n" +
            "Password must contain at least one lowercase Latin character [a-z].\n" +
            "Password must contain at least one uppercase Latin character [A-Z].\n" +
            "Password must contain at least one special character like ! @ # & ( ).\n" +
            "Password must contain a length of at least 8 characters and a maximum of 20 characters.";

    @NotEmpty(message = "Password confirmation can't be empty!")
    @Pattern( regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8}$",
            message = SECURE_PASSWORD_REQUIREMENTS)
    private String passwordConfirm;


    @Email
    private String email;

    @Size(min = 10, max = 10, message = "A phone number must contain 10 digits.")
    private String phoneNumber;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
