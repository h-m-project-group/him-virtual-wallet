package him.virtualwallet.models.dtos;

public class TransactionFilterDto {

    private String date;
    private String sender;
    private String recipient;
    private String transactionType;
    private String sortOption;

    public TransactionFilterDto() {
    }

    public TransactionFilterDto(String date,
                                String sender,
                                String recipient,
                                String transactionType,
                                String sortOption) {
        this.date = date;
        this.sender = sender;
        this.recipient = recipient;
        this.transactionType = transactionType;
        this.sortOption = sortOption;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getSortOption() {
        return sortOption;
    }

    public void setSortOption(String sortOption) {
        this.sortOption = sortOption;
    }
}
