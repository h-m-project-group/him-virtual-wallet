package him.virtualwallet.models.dtos;

import javax.validation.constraints.Digits;

public class DepositDto {

    @Digits(integer = 16, fraction = 0)
    private long cardNumber;

    @Digits(integer = 3,fraction = 0)
    private int checkNumber;

    @Digits(integer = 5,fraction = 2,message = "The maximum amount allowed for transfer is 99,999.99")
    private double depositSum;

    public DepositDto(){
    }

    public DepositDto(double depositSum){
        this.depositSum=depositSum;
    }

    public void setDepositSum(double depositSum) {
        this.depositSum = depositSum;
    }

    public double getDepositSum() {
        return depositSum;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(int checkNumber) {
        this.checkNumber = checkNumber;
    }
}
