package him.virtualwallet.models.dtos;

public class CustomerSearchDto {

    String searchTerm;

    public CustomerSearchDto() {
    }

    public CustomerSearchDto(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    //    public CustomerSearchDto(String username, String email, String phoneNumber) {
//        this.username = username;
//        this.email = email;
//        this.phoneNumber = phoneNumber;
//    }
//
//    // Getters
//
//    public String getUsername() {
//        return username;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public String getPhoneNumber() {
//        return phoneNumber;
//    }
//
//    // Setters
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }
}
