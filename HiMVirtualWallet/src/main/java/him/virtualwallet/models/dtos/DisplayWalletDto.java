package him.virtualwallet.models.dtos;

public class DisplayWalletDto {

    private int walletID;
    private int userID;
    private double amount;

    public DisplayWalletDto() {
    }

    public int getWalletID() {
        return walletID;
    }

    public void setWalletID(int walletID) {
        this.walletID = walletID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
