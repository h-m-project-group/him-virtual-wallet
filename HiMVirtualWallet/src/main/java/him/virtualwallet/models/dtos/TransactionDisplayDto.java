package him.virtualwallet.models.dtos;

import him.virtualwallet.enums.TransactionStatusEnum;
import him.virtualwallet.enums.TransactionTypeEnum;
import him.virtualwallet.models.User;

import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

public class TransactionDisplayDto {

    @Positive(message = "Transaction ID is always a positive number")
    private int transactionID;

    private String sender;

    private String recipient;

    private double transferSum;

    private String transactionType;

    private String date;

    private String transactionStatus;

    public TransactionDisplayDto() {
    }

    public TransactionDisplayDto(int transactionID,
                                 String sender,
                                 String recipient,
                                 double transferSum,
                                 String transactionType,
                                 String date,
                                 String transactionStatus) {
        this.transactionID = transactionID;
        this.sender = sender;
        this.recipient = recipient;
        this.transferSum = transferSum;
        this.transactionType = transactionType;
        this.date = date;
        this.transactionStatus = transactionStatus;
    }

    public int getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public double getTransferSum() {
        return transferSum;
    }

    public void setTransferSum(double transferSum) {
        this.transferSum = transferSum;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
