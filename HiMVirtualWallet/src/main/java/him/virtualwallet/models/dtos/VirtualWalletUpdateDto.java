package him.virtualwallet.models.dtos;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Positive;

public class VirtualWalletUpdateDto {

    private int walletID;

    @Digits(integer = 5,fraction = 2)
    private double amount;

    public VirtualWalletUpdateDto() {
    }

    public VirtualWalletUpdateDto(int walletID, double amount) {
        this.walletID = walletID;
        this.amount = amount;
    }

    // Getters

    public int getWalletID() {
        return walletID;
    }

    public double getAmount() {
        return amount;
    }

    // Setters

    public void setWalletID(int walletID) {
        this.walletID = walletID;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
