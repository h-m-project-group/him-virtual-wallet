package him.virtualwallet.models.dtos;

import him.virtualwallet.models.User;

import javax.persistence.*;
import java.util.Date;

public class SearchCardDto {

    private int cardID;
    private String cardType;
    private long cardNumber;
    private Date expirationDate;
    private String cardHolder;
    private int checkNumber;
    private int userID;

    public SearchCardDto() {
    }

    // Getters

    public int getCardID() {
        return cardID;
    }

    public String getCardType() {
        return cardType;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public int getCheckNumber() {
        return checkNumber;
    }

    public int getUserID() {
        return userID;
    }

    // Setters

    public void setCardID(int cardID) {
        this.cardID = cardID;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public void setCheckNumber(int checkNumber) {
        this.checkNumber = checkNumber;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

}
