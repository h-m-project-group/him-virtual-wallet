package him.virtualwallet.models.dtos;

import javax.validation.constraints.*;

public class CardDto {

    @Digits(integer = 16, fraction = 0)
    @NotNull
    private long cardNumber;

    @NotBlank
    private String expirationDate;

    @NotBlank
    private String cardHolder;

    @Digits(integer = 3, fraction = 0)
    private int checkNumber;

    public CardDto() {
    }

    public CardDto(long cardNumber, String expirationDate, String cardHolder, int checkNumber) {
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cardHolder = cardHolder;
        this.checkNumber = checkNumber;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public int getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(int checkNumber) {
        this.checkNumber = checkNumber;
    }
}
