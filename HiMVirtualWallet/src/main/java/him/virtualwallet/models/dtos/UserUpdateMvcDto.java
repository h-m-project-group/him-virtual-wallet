package him.virtualwallet.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserUpdateMvcDto {

    @NotBlank
    @Size(min = 5, max = 30, message = "Please enter an email with characters between 3 and 30")
    private String email;

    @Size(min = 10, max = 10, message = "A phone number must contain 10 digits.")
    private String phoneNumber;

    @NotBlank
    private String userStatus;

    public UserUpdateMvcDto() {
    }

    public UserUpdateMvcDto(String email, String phoneNumber, String userStatus) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.userStatus = userStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
}
