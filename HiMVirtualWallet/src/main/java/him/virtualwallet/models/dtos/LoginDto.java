package him.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class LoginDto {
    private static final String SECURE_PASSWORD_REQUIREMENTS = "Secure Password requirements\n" +
            "\n" +
            "Password must contain at least one digit [0-9].\n" +
            "Password must contain at least one lowercase Latin character [a-z].\n" +
            "Password must contain at least one uppercase Latin character [A-Z].\n" +
            "Password must contain at least one special character like ! @ # & ( ).\n" +
            "Password must contain a length of at least 8 characters and a maximum of 20 characters.";

    @NotEmpty
    private String username;

    @NotEmpty
    @Pattern( regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8}$",
            message = SECURE_PASSWORD_REQUIREMENTS)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
