package him.virtualwallet.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CustomerDto {

    public static final String PASSWORD_RESTRICTIONS_MESSAGE = "Password must be at least 8 characters and contain at least one of each: small letter, capital letter, number, special symbol";
    @NotBlank
    @Size(min = 2, max = 20, message = "Please enter a username with characters between 3 and 30")
    private String username;

    @NotBlank
    @Size(min = 8, max = 15, message = "A password must contain between 8 and 10 digits.")
//    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{​​​​​​8,}​​​​​​$")

//    String regex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$";

//    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$",
//            message = PASSWORD_RESTRICTIONS_MESSAGE
//    )
    private String password;

    @NotBlank
    @Size(min = 5, max = 30, message = "Please enter an email with characters between 3 and 30")
    private String email;

    @Size(min = 10, max = 10, message = "A phone number must contain 10 digits.")
    private String phoneNumber;

    @NotBlank
    private String verificationType;

    @NotBlank
    private String userStatus;

    public CustomerDto(String username,
                       String password,
                       String email,
                       String phoneNumber,
                       String verificationType,
                       String userStatus) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.verificationType = verificationType;
        this.userStatus=userStatus;
    }

    public CustomerDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getVerificationType() {
        return verificationType;
    }

    public void setVerificationType(String verificationType) {
        this.verificationType = verificationType;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

}
