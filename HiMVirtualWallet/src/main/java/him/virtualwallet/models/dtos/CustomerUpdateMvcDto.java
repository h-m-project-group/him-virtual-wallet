package him.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CustomerUpdateMvcDto {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;

    @NotEmpty(message = "Password confirmation can't be empty!")
    private String passwordConfirm;

    @Size(min = 5, max = 30, message = "Please enter an email with characters between 3 and 30")
    private String email;

    @Size(min = 10, max = 10, message = "A phone number must contain 10 digits.")
    private String phoneNumber;

    public CustomerUpdateMvcDto() {
    }

    public CustomerUpdateMvcDto(String username,
                                String password,
                                String passwordConfirm,
                                String email,
                                String phoneNumber) {
        this.username = username;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
