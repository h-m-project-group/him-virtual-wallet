package him.virtualwallet.models;

import him.virtualwallet.enums.TransactionStatusEnum;
import him.virtualwallet.enums.TransactionTypeEnum;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private int transactionID;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id")
    private User recipient;

    @Column(name = "transfer_sum")
    private double transferSum;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type")
    private TransactionTypeEnum transactionType;

    @Column(name = "date")
    private LocalDateTime date;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_status_type")
    private TransactionStatusEnum transactionStatus;

    public Transaction() {
    }

    public Transaction(int transactionID,
                       User sender,
                       User recipient,
                       double transferSum,
                       TransactionTypeEnum transactionType,
                       LocalDateTime date,
                       TransactionStatusEnum transactionStatus) {
        this.transactionID = transactionID;
        this.sender = sender;
        this.recipient = recipient;
        this.transferSum = transferSum;
        this.transactionType = transactionType;
        this.date = date;
        this.transactionStatus = transactionStatus;
    }

    // Getters

    public int getTransactionID() {
        return transactionID;
    }

    public User getSender() {
        return sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public double getTransferSum() {
        return transferSum;
    }

    public TransactionTypeEnum getTransactionType() {
        return transactionType;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public TransactionStatusEnum getTransactionStatus() {
        return transactionStatus;
    }

    //Setters

    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public void setTransferSum(double transferSum) {
        this.transferSum = transferSum;
    }

    public void setTransactionType(TransactionTypeEnum transactionType) {
        this.transactionType = transactionType;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setTransactionStatus(TransactionStatusEnum transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
