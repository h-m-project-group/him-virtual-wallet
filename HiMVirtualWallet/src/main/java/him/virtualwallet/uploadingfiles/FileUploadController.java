//package him.virtualwallet.uploadingfiles;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//
//import him.virtualwallet.controllers.AuthenticationHelper;
//import him.virtualwallet.exceptions.StorageFileNotFoundException;
//import him.virtualwallet.models.User;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.io.Resource;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//import him.virtualwallet.uploadingfiles.storage.StorageService;
//import javax.servlet.http.HttpSession;
//
//
//@Controller
//public class FileUploadController {
//
//    private final StorageService storageService;
//    private final AuthenticationHelper authenticationHelper;
//
//    @Autowired
//    public FileUploadController(StorageService storageService, AuthenticationHelper authenticationHelper) {
//
//        this.storageService = storageService;
//        this.authenticationHelper = authenticationHelper;
//    }
//
//    @GetMapping("/uploads")
//    public String listUploadedFiles(Model model) throws IOException {
//
////        model.addAttribute("files", storageService.loadAll().map(
////                path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
////                        "serveFile", path.getFileName().toString()).build().toUri().toString())
////                .collect(Collectors.toList()));
//
////        return "uploadForm";
//        return "uploads";
////        return "customer-account";
//    }
//
//    @GetMapping("/files/{filename:.+}")
//    @ResponseBody
//    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {
//
//        Resource file = storageService.loadAsResource(filename);
//
//        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
//                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
//    }
//
//    @PostMapping("/uploads")
//    public String handleFileUpload(@RequestParam("file") MultipartFile file,
//                                   RedirectAttributes redirectAttributes,
//                                   HttpSession session) throws IOException {
//
//        User user = authenticationHelper.tryGetUser(session);
//
//        storageService.store(file, user);
//        redirectAttributes.addFlashAttribute("message",
//                "You successfully uploaded " + file.getOriginalFilename() + "!");
//
//        FileInputStream inputStream = (FileInputStream) file.getInputStream();
//
//        return "redirect:/uploads";
////        return "redirect:/users/uploads";
//    }
//
//    @ExceptionHandler(StorageFileNotFoundException.class)
//    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
//
//        return ResponseEntity.notFound().build();
//    }
//
//}
