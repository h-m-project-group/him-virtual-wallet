package him.virtualwallet.uploadingfiles.storage;

import him.virtualwallet.controllers.mvc.CustomerMvcController;
import him.virtualwallet.exceptions.StorageException;
import him.virtualwallet.exceptions.StorageFileNotFoundException;
import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;
    private final UserRepository userRepository;

    @Autowired
    public FileSystemStorageService(StorageProperties properties, UserRepository userRepository) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.userRepository = userRepository;
    }

    @Override
    public void store(MultipartFile file, User user) throws IOException {

        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
            }

            Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
        }

        List<String> photoUrlList = loadAll().map(
                path -> MvcUriComponentsBuilder.fromMethodName(CustomerMvcController.class,
                        "serveFile", path.getFileName().toString()).build().toUri().toString())
                .collect(Collectors.toList());

        String photoUrl = photoUrlList.get(photoUrlList.size() - 1);

        user.setPhotoUrl(photoUrl);

        userRepository.update(user);
    }

    @Override
    public Stream<Path> loadAll() {

        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(path -> this.rootLocation.relativize(path));
        }
        catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {

        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            }

            else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);
            }

        }

        catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {

        try {
            Files.createDirectory(rootLocation);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

}

