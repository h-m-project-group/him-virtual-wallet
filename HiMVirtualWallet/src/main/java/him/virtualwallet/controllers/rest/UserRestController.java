package him.virtualwallet.controllers.rest;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.mappers.UserMapper;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VirtualWallet;
import him.virtualwallet.models.dtos.CustomerDisplayDto;
import him.virtualwallet.models.dtos.CustomerDto;
import him.virtualwallet.models.dtos.RegisterDto;
import him.virtualwallet.services.contracts.UserService;
import him.virtualwallet.services.contracts.VirtualWalletService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final VirtualWalletService virtualWalletService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRestController(UserService userService, UserMapper userMapper, VirtualWalletService virtualWalletService, AuthenticationHelper authenticationHelper) {

        this.userMapper = userMapper;
        this.userService = userService;
        this.virtualWalletService = virtualWalletService;
        this.authenticationHelper = authenticationHelper;
    }

    @ApiOperation(value = "Returns all users")
    @GetMapping
    public List<User> getAllUsers(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAll(user);
    }

    @ApiOperation(value = "Returns a user by their ID")
    @GetMapping("/{id}")
    public User getUserByID(@RequestHeader HttpHeaders headers, @PathVariable int id) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            return userService.getByID(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a new customer")
    @PostMapping("/customer")
    public User createCustomer(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody RegisterDto dto) {

        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            User customer = userMapper.fromRegisterDto(dto);
            userService.create(customer, authenticatedUser);

            VirtualWallet virtualWallet = new VirtualWallet();
            virtualWallet.setUser(customer);
            virtualWalletService.create(virtualWallet,authenticatedUser);

            return customer;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Creates a new Admin")
    @PostMapping("/admin")
    public Admin createAdmin(@RequestHeader HttpHeaders headers,
                             @Valid @RequestBody RegisterDto dto) {

        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            Admin admin = userMapper.fromRegisterDtoToAdmin(dto);
            userService.create(admin, authenticatedUser);


            return admin;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Updates an existing user")
    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody CustomerDto customerDto) {

        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            User customer = userMapper.fromDto(customerDto, id);
            userService.update(customer, authenticatedUser);
            return customer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Deletes a customer")
    @DeleteMapping("/{id}")
    public void deleteCustomer(@RequestHeader HttpHeaders headers,
                               @PathVariable int id) {

        try {
            User customer = authenticationHelper.tryGetUser(headers);
            userService.delete(id, customer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Allows searching users by username, phone number or email")
    @GetMapping("/search")
    public List<User> search(@RequestHeader HttpHeaders headers,
                               @RequestParam(required = false) String searchTerm){
        if (searchTerm == null) {
            return new ArrayList<>();
        }
        try{
            User user = authenticationHelper.tryGetUser(headers);
            return userService.search(searchTerm,user);
        }catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Filters customers by username, email or phone number.")
    @GetMapping("/filter")
    public List<CustomerDisplayDto> filter(@RequestHeader HttpHeaders headers,
                                           @RequestParam(required = false) String username,
                                           @RequestParam(required = false) String email,
                                           @RequestParam(required = false) String phoneNumber) {

        try {
            User user = authenticationHelper.tryGetUser(headers);

            return userService.filter(Optional.ofNullable(username),
                    Optional.ofNullable(email), Optional.ofNullable(phoneNumber), user)
                    .stream()
                    .map((User customer) -> userMapper.fromUserToCustomerDisplayDto(customer))
                    .collect(Collectors.toList());

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
