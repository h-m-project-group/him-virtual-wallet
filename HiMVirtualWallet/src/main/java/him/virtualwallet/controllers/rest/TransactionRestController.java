package him.virtualwallet.controllers.rest;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.mappers.TransactionMapper;
import him.virtualwallet.models.Transaction;
import him.virtualwallet.models.User;
import him.virtualwallet.models.dtos.DepositDto;
import him.virtualwallet.models.dtos.TransactionDto;
import him.virtualwallet.services.contracts.TransactionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/transactions")
public class TransactionRestController {

    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper authenticationHelper;
    private final RestTemplate restTemplate;

    public TransactionRestController(TransactionService transactionService,
                                     TransactionMapper transactionMapper,
                                     AuthenticationHelper authenticationHelper,
                                     RestTemplate restTemplate) {

        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.authenticationHelper = authenticationHelper;
        this.restTemplate = restTemplate;
    }
    @ApiOperation(value = "Returns all transactions")
    @GetMapping
    public List<Transaction> getAllTransactions(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.getAll(user);
    }

    @ApiOperation(value = "Returns a transaction by its ID")
    @GetMapping("/{id}")
    public Transaction getTransactionById(@RequestHeader HttpHeaders headers,
                                          @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return transactionService.getByID(id, user);
    }

    @ApiOperation(value = "Creates a transaction")
    @PostMapping("/new")
    public Transaction createTransaction(@RequestHeader HttpHeaders headers,
                                         @Valid @RequestBody TransactionDto transactionDto) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Transaction transaction = transactionMapper.fromDtoWithUser(transactionDto, user);
            transactionService.create(transaction, user);

            return transaction;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Adds funds to wallet")
    @PostMapping("/deposit")
    public Transaction createDeposit(@RequestHeader HttpHeaders headers,
                                     @Valid @RequestBody DepositDto depositDto) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Transaction depositTransaction = transactionMapper.fromDepositDtoWithUser(depositDto, user);

            restTemplate.getForObject("http://localhost:8080/api/dummy-bank", HttpStatus.class);

            transactionService.createDeposit(depositTransaction, user);

            return depositTransaction;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Filters and sorts transactions")
    @GetMapping("/filter")
    public List<Transaction> filter(@RequestHeader HttpHeaders headers,
                                    @RequestParam(required = false) String sender,
                                    @RequestParam(required = false) String recipient,
                                    @RequestParam(required = false) String transferDirection,
                                    @RequestParam(required = false) String period,
                                    @RequestParam(required = false) String sortOption) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return transactionService.filter(Optional.ofNullable(period),
                    Optional.ofNullable(sender),
                    Optional.ofNullable(recipient),
                    Optional.ofNullable(transferDirection),
                    Optional.ofNullable(sortOption),
                    user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    e.getMessage());
        }
    }
}