package him.virtualwallet.controllers.rest;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.mappers.CardMapper;
import him.virtualwallet.models.Card;
import him.virtualwallet.models.dtos.CardDto;
import him.virtualwallet.models.User;
import him.virtualwallet.services.contracts.CardService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/cards")
public class CardRestController {

    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardRestController(CardService cardService, CardMapper cardMapper, AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ApiOperation(value = "Returns all cards")
    @GetMapping
    public List<Card> getAllCards(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return cardService.getAll(user);
    }

    @ApiOperation(value = "Returns a card by its ID")
    @GetMapping("/{id}")
    public Card getCardById(@RequestHeader HttpHeaders headers,
                            @PathVariable int id){
        User user = authenticationHelper.tryGetUser(headers);
        return cardService.getByID(id,user);
    }

    @ApiOperation(value = "Creates a credit/debit card.")
    @PostMapping
    public Card createCard(@RequestHeader HttpHeaders headers,
                                   @Valid @RequestBody CardDto cardDto) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Card card = cardMapper.fromDto(cardDto, user);
            cardService.create(card, user);
            return card;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Updates a card.")
    @PutMapping("/{id}")
    public Card updateCard(@RequestHeader HttpHeaders headers,
                                   @PathVariable int id,
                                   @Valid @RequestBody CardDto cardDto) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            Card card = cardMapper.fromDto(cardDto, id);
            cardService.update(card, user);

            return card;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }

    @ApiOperation(value = "Deletes a card.")
    @DeleteMapping("/{id}")
    public void deleteCard(@RequestHeader HttpHeaders headers,
                               @PathVariable int id) {

        try {

            User user = authenticationHelper.tryGetUser(headers);
            cardService.delete(id,user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }
    }
}
