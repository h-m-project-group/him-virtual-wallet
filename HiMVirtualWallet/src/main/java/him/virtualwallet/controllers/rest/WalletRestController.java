package him.virtualwallet.controllers.rest;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.mappers.WalletMapper;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VirtualWallet;
import him.virtualwallet.models.dtos.VirtualWalletDto;
import him.virtualwallet.models.dtos.VirtualWalletUpdateDto;
import him.virtualwallet.services.contracts.VirtualWalletService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/wallets")
public class WalletRestController {

    private final AuthenticationHelper authenticationHelper;
    private final VirtualWalletService walletService;
    private final WalletMapper walletMapper;

    @Autowired
    public WalletRestController(AuthenticationHelper authenticationHelper,
                                VirtualWalletService walletService,
                                WalletMapper walletMapper) {

        this.authenticationHelper = authenticationHelper;
        this.walletService = walletService;
        this.walletMapper = walletMapper;
    }

    @ApiOperation(value = "Returns all wallets")
    @GetMapping
    public List<VirtualWallet> getAllWallets(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return walletService.getAll(user);
        }

        @GetMapping("/{id}")
        public VirtualWallet getWalletById(@RequestHeader HttpHeaders headers,
                                           @PathVariable int id){
            User user = authenticationHelper.tryGetUser(headers);
            return walletService.getByID(id, user);
        }

        @ApiOperation(value = "Creates a virtual wallet")
        @PostMapping
        public VirtualWallet createWallet(@RequestHeader HttpHeaders headers,
                               @Valid @RequestBody VirtualWalletDto walletDto) {

            try {
                User user = authenticationHelper.tryGetUser(headers);
                VirtualWallet wallet = walletMapper.fromDto(walletDto);
                walletService.create(wallet, user);
                return wallet;

            } catch (DuplicateEntityException e) {

                throw new ResponseStatusException(
                        HttpStatus.CONFLICT,
                        e.getMessage());

            } catch (UnauthorizedOperationException e) {

                throw new ResponseStatusException(
                        HttpStatus.UNAUTHORIZED,
                        e.getMessage()
                );
            }
        }

        @ApiOperation(value = "Adds money to wallet")
        @PutMapping("/{id}/deposit")
        public VirtualWallet depositInWallet(@RequestHeader HttpHeaders headers,
                               @PathVariable int id,
                               @Valid @RequestBody VirtualWalletUpdateDto walletDto) {

            try {
                User user = authenticationHelper.tryGetUser(headers);
                VirtualWallet wallet = walletMapper.depositUpdate(walletDto, id, user);
                walletService.update(wallet, user);

                return wallet;
            }
            catch (EntityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
            catch (DuplicateEntityException e) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
            }
            catch (UnauthorizedOperationException e) {
                throw new ResponseStatusException(
                        HttpStatus.UNAUTHORIZED,
                        e.getMessage()
                );
            }

        }

    @ApiOperation(value = "Withdraws money from a wallet.")
    @PutMapping("/{id}/withdraw")
    public VirtualWallet withdrawFromWallet(@RequestHeader HttpHeaders headers,
                                         @PathVariable int id,
                                         @Valid @RequestBody VirtualWalletUpdateDto walletDto) {

        try {
            User user = authenticationHelper.tryGetUser(headers);
            VirtualWallet wallet = walletMapper.withdrawUpdate(walletDto, id, user);
            walletService.update(wallet, user);

            return wallet;
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED,
                    e.getMessage()
            );
        }

    }

        @ApiOperation(value = "Deletes a wallet.")
        @DeleteMapping("/{id}")
        public void deleteWallet(@RequestHeader HttpHeaders headers,
                                 @PathVariable int id) {

            try {
                User user = authenticationHelper.tryGetUser(headers);
                walletService.delete(id, user);
            }
            catch (EntityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
            catch (UnauthorizedOperationException e) {
                throw new ResponseStatusException(
                        HttpStatus.UNAUTHORIZED,
                        e.getMessage()
                );
            }
        }
}
