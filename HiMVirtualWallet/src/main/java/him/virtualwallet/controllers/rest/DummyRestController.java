package him.virtualwallet.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/api/dummy-bank")
public class DummyRestController {

    private static final String TRANSACTION_NOT_ALLOWED = "Transaction was cancelled. Please check your funds.";
    private final Random random;

    @Autowired
    public DummyRestController(Random random) {
        this.random = random;
    }

    @ApiOperation(value = "Dummy bank returns HttpStatus")
    @GetMapping
    public HttpStatus getRandom() {
        int randomNumber = random.nextInt();
            if (randomNumber % 2 == 0) {
                return HttpStatus.OK;
            } else {
                return HttpStatus.BAD_GATEWAY;
            }
    }
}
