package him.virtualwallet.controllers.mvc;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.enums.VerificationTypeEnum;
import him.virtualwallet.exceptions.AuthenticationFailureException;
import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.mappers.UserMapper;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VerificationToken;
import him.virtualwallet.models.dtos.LoginDto;
import him.virtualwallet.models.dtos.RegisterDto;
import him.virtualwallet.services.OnRegistrationCompleteEvent;
import him.virtualwallet.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ApplicationEventPublisher eventPublisher;
    private final MessageSource messages;

    @Autowired
    public AuthenticationController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper, ApplicationEventPublisher eventPublisher, MessageSource messages) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.eventPublisher = eventPublisher;
        this.messages = messages;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto loginDto,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "login";
        }
        try {
            authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            session.setAttribute("currentUser", loginDto.getUsername());
            return "redirect:/logged";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "authentication_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }


    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto registerDto,
                                 BindingResult bindingResult,
                                 HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm",
                    "password_error",
                    "Password confirmation does not match password!");
            return "register";
        }
        try {
            User user = userMapper.fromRegisterDto(registerDto);

            userService.registerCustomer(user);
            String appUrl = request.getContextPath();
            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user,
                    request.getLocale(), appUrl));
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
    }

    @GetMapping("/registrationConfirm")
    public String confirmRegistration(Model model,
                                      @RequestParam("token") String token) {

        try {
            VerificationToken verificationToken = userService.getVerificationToken(token);

            Customer user = userService.getCustomer(token);
//            Calendar cal = Calendar.getInstance();
            if (!verificationToken.getToken().equals(token)) {
//        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) < 0) {
//            String messageValue = messages.getMessage("auth.message.expired", null, locale);
//                String messageValue = "The token has expired. Please request another one!";
//                model.addAttribute("message", messageValue);
                return "bad-user";
            }

            user.setVerificationType(VerificationTypeEnum.VERIFIED);
            userService.updateUserVerification(user);
            return "registration-confirm";
        } catch (EntityNotFoundException e) {
            String message = "Invalid token!";
            model.addAttribute("message", message);
            return "bad-user";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

}
