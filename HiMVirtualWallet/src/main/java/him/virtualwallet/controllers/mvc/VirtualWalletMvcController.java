package him.virtualwallet.controllers.mvc;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.AuthenticationFailureException;
import him.virtualwallet.exceptions.DummyBankServerException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.mappers.TransactionMapper;
import him.virtualwallet.mappers.WalletMapper;
import him.virtualwallet.models.Card;
import him.virtualwallet.models.Transaction;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VirtualWallet;
import him.virtualwallet.models.dtos.DepositDto;
import him.virtualwallet.models.dtos.DisplayWalletDto;
import him.virtualwallet.services.contracts.CardService;
import him.virtualwallet.services.contracts.TransactionService;
import him.virtualwallet.services.contracts.UserService;
import him.virtualwallet.services.contracts.VirtualWalletService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/wallets")
public class VirtualWalletMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final VirtualWalletService walletService;
    private final WalletMapper walletMapper;
    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final RestTemplate restTemplate;

    private final CardService cardService;

    @Autowired
    public VirtualWalletMvcController(AuthenticationHelper authenticationHelper,
                                      UserService userService,
                                      VirtualWalletService walletService,
                                      WalletMapper walletMapper,
                                      TransactionService transactionService,
                                      TransactionMapper transactionMapper,
                                      RestTemplate restTemplate,
                                      CardService cardService) {

        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.walletService = walletService;
        this.walletMapper = walletMapper;
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.restTemplate = restTemplate;
        this.cardService = cardService;
    }

    @GetMapping
    public String showAllWallets(Model model, HttpSession session) {

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            model.addAttribute("allWallets", walletService.getAll(currentUser));

            return "wallets";

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleWallet(@PathVariable int id,
                                   Model model,
                                   HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            VirtualWallet wallet = walletService.getByID(id, user);

            DisplayWalletDto displayWalletDto = walletMapper.fromWalletToDisplayWalletDto(wallet);

            model.addAttribute("displayWalletDto", displayWalletDto);
            model.addAttribute("depositDto", new DepositDto());
            model.addAttribute("user", user);

            return "wallet";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/deposit")
    public String showEditDepositPage(@PathVariable int id,
                                      Model model,
                                      HttpSession session) {

        User authenticatedUser;

        try {
            authenticatedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            VirtualWallet wallet = walletService.getByUserID(authenticatedUser.getUserID());
            model.addAttribute("wallet", wallet);
            model.addAttribute("depositDto", new DepositDto());

            return "deposit";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @ApiOperation(value = "Deposits in a wallet.")
    @PostMapping("/{id}/deposit")
    public String makeDeposit(@PathVariable int id,
                              @Valid @ModelAttribute("depositDto") DepositDto depositDto,
                              BindingResult errors,
                              Model model,
                              HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("user", user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "deposit";
        }
        try {
            Transaction depositTransaction = transactionMapper
                    .fromDepositDtoWithUser(depositDto, user);

            transactionService.createDeposit(depositTransaction, user);

            VirtualWallet wallet = walletService
                    .getByUserID(user.getUserID());
            model.addAttribute("wallet", wallet);

            return "redirect:/wallets/customer-wallet";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }  catch (DummyBankServerException e) {
            return "try-again";
        }
    }

    @GetMapping("/customer-wallet")
    public String showCustomerWallet(Model model,
                                     HttpSession session) {

        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getByID(authenticatedUser.getUserID(), authenticatedUser);
            VirtualWallet wallet = walletService.getByUserID(user.getUserID());
            model.addAttribute("wallet", wallet);
            model.addAttribute("depositDto", new DepositDto());

            model.addAttribute("customer", userService.getCustomerInfoByID(authenticatedUser.getUserID(), authenticatedUser));

            return "customer-wallet";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @ModelAttribute("cards")
    public List<Card> populateCustomerCards(HttpSession session) {

        User currentUser = authenticationHelper.tryGetUser(session);

        return cardService.getAllCustomerCards(currentUser);
    }

}
