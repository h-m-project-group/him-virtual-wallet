package him.virtualwallet.controllers.mvc;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.AuthenticationFailureException;
import him.virtualwallet.exceptions.DuplicateEntityException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.mappers.CardMapper;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.Card;
import him.virtualwallet.models.User;
import him.virtualwallet.models.dtos.CardDto;
import him.virtualwallet.models.dtos.SearchCardDto;
import him.virtualwallet.services.contracts.CardService;
import him.virtualwallet.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/cards")
public class CardMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final CardService cardService;
    private final CardMapper cardMapper;

    @Autowired
    public CardMvcController(AuthenticationHelper authenticationHelper,
                             CardService cardService,
                             CardMapper cardMapper) {

        this.authenticationHelper = authenticationHelper;
        this.cardService = cardService;
        this.cardMapper = cardMapper;
    }

    @GetMapping
    public String showAllCards(Model model, HttpSession session) {

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            if (currentUser.getClass() == Admin.class) {
                model.addAttribute("cards", cardService.getAll(currentUser));
                model.addAttribute("cardDto",
                        new CardDto());

                return "cards";
            } else {
                model.addAttribute("customerCards",
                        cardService.getAllCustomerCards(currentUser));
                model.addAttribute("cardDto",
                        new CardDto());

                return "customer-cards";
            }
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleCard(@PathVariable int id,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Card card = cardService.getByID(id, user);
            model.addAttribute("user", user);
            model.addAttribute("card", card);

            return "card";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String showNewCardPage(Model model, HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("cardDto", new CardDto());
        return "card-new";
    }

    @PostMapping("/new")
    public String createCard(@Valid @ModelAttribute("cardDto") CardDto cardDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "card-new";
        }
        try {
            Card card = cardMapper.fromDto(cardDto, user);
            cardService.create(card, user);
            model.addAttribute("card",card);
            model.addAttribute("user",user);
            return "card";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_card", e.getMessage());
            return "card-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditCardPage(@PathVariable int id,
                                   Model model,
                                   HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Card card = cardService.getByID(id, user);
            CardDto cardDto = cardMapper.fromCardToDto(card);

            model.addAttribute("cardID", id);
            model.addAttribute("cardDto", cardDto);

            return "card-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping(value = "/{id}/update")
    public String updateCard(@PathVariable int id,
                             @Valid @ModelAttribute("cardDto") CardDto cardDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
            model.addAttribute("user", user);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "card-update";
        }

        try {
            Card card = cardMapper.fromDto(cardDto, id);
            cardService.update(card, user);

            return "redirect:/cards";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_card", e.getMessage());
            return "card-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping(value = "/{id}/delete", params = "delete") // ??
    public String deleteCard(@PathVariable int id,
                             Model model,
                             HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            cardService.delete(id, user);
            return "redirect:/cards";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }


///////////////

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

//////////////////


//    @ModelAttribute("cards")
//    public List<Card> populateCards(HttpSession session) {
//        User user = authenticationHelper.tryGetUser(session);
//        return cardService.getAll(user);
//    }

}
