package him.virtualwallet.controllers.mvc;

import him.virtualwallet.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private UserService userService;

    public HomeMvcController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String showHomePage() {
        return "index";
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }
}
