package him.virtualwallet.controllers.mvc;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.enums.SortTransactionOptions;
import him.virtualwallet.enums.TransactionTypeEnum;
import him.virtualwallet.exceptions.AuthenticationFailureException;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.exceptions.InsufficientBalanceException;
import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.mappers.TransactionMapper;
import him.virtualwallet.models.Transaction;
import him.virtualwallet.models.User;
import him.virtualwallet.models.dtos.*;
import him.virtualwallet.services.contracts.TransactionService;
import him.virtualwallet.services.contracts.UserService;
import him.virtualwallet.services.contracts.VirtualWalletService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/transactions")
public class TransactionsMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final TransactionMapper transactionMapper;
    private final TransactionService transactionService;
    private final UserService userService;
    private final VirtualWalletService walletService;

    public TransactionsMvcController(AuthenticationHelper authenticationHelper,
                                     TransactionMapper transactionMapper,
                                     TransactionService transactionService, UserService userService, VirtualWalletService walletService) {
        this.authenticationHelper = authenticationHelper;
        this.transactionMapper = transactionMapper;
        this.transactionService = transactionService;
        this.userService = userService;
        this.walletService = walletService;
    }

    @GetMapping
    public String showAllTransactions(Model model, HttpSession session) {

        User currentUser;

        try {
            currentUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            model.addAttribute("transactionFilterDto", new TransactionFilterDto());
            model.addAttribute("transactionDto", new TransactionDto());
            model.addAttribute("currentUser", currentUser);


            if (currentUser.getClass().getSimpleName().equals("Admin")) {
                model.addAttribute("transactions", transactionService.getAll(currentUser));
                return "transactions-admin";
            } else {

                model.addAttribute("customerTransactions", transactionService.viewUserTransactions(currentUser.getUsername()));
                model.addAttribute("customer", userService.getCustomerInfoByID(currentUser.getUserID(), currentUser));
                return "transactions-customer";
            }

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (EntityNotFoundException e){
            return "no-transactions";
        }
    }

    @GetMapping("/{id}")
    public String showSingleTransaction(@PathVariable int id,
                                        Model model,
                                        HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Transaction transaction = transactionService.getByID(id, user);
            TransactionDisplayDto transactionDto = transactionMapper.fromTransactionToDto(transaction);

            model.addAttribute("transactionDto", transactionDto);
            model.addAttribute("user", user);

            return "transaction";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String showNewTransactionPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("transactionDto", new TransactionDto());

        return "transaction-new";

    }

    @PostMapping("/new")
    public String createTransaction(@Valid @ModelAttribute("transactionDto")
                                            TransactionDto transactionDto,
                                    BindingResult errors,
                                    Model model,
                                    HttpSession session) {

        User authUser;
        try {
            authUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "transaction-new";
        }
        try {
            Transaction transaction = transactionMapper.fromDtoWithUser(transactionDto, authUser);
            model.addAttribute("transactionDto", transactionDto);
            transactionService.create(transaction, authUser);

            return "redirect:/transactions";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        catch (InsufficientBalanceException e) {
            model.addAttribute("error", e.getMessage());
            model.addAttribute("wallet", walletService.getByUserID(authUser.getUserID()));
            return "insufficient-funds";
        }
    }

    @PostMapping("/filter")
    public String filterTransactions(@Valid @ModelAttribute("transactionFilterDto") TransactionFilterDto transactionFilterDto,
                                     BindingResult errors,
                                     Model model,
                                     HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "transactions-customer";
        }
        try {
            Optional<String> date = (transactionFilterDto.getDate() != null && !transactionFilterDto.getDate().equals(""))
                    ? Optional.ofNullable(transactionFilterDto.getDate())
                    : Optional.empty();

            Optional<String> sender = (transactionFilterDto.getSender() != null && !transactionFilterDto.getSender().equals(""))
                    ? Optional.ofNullable(transactionFilterDto.getSender())
                    : Optional.empty();

            Optional<String> recipient = !transactionFilterDto.getRecipient().equals("")
                    ? Optional.ofNullable(transactionFilterDto.getRecipient())
                    : Optional.empty();

            Optional<String> transactionType = (!transactionFilterDto.getTransactionType().equals("-1"))
                    ? Optional.ofNullable(transactionFilterDto.getTransactionType())
                    : Optional.empty();

            Optional<String> sortOption = (!transactionFilterDto.getSortOption().equals("-1"))
                    ? Optional.ofNullable(transactionFilterDto.getSortOption())
                    : Optional.empty();

            var filtered = transactionService
                    .filter(date, sender, recipient, transactionType, sortOption, user);

            model.addAttribute("transactions", filtered);
            model.addAttribute("customerTransactions", filtered);
            model.addAttribute("currentUser", user);

            if (user.getClass().getSimpleName().equals("Customer")) {
                model.addAttribute("customer", userService.getCustomerInfoByID(user.getUserID(), user));
                return "transactions-customer";
            } else {
                return "transactions-admin";
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping(value = "/page/{pageID}")
    public String pagination(@Valid @ModelAttribute("transactionFilterDto")
                                     TransactionFilterDto transactionFilterDto,
                             @PathVariable int pageID,
                             Model model,
                             HttpSession session) {
        User loggedUser;
        int total = 10;

        model.addAttribute("pageID", pageID);


        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("customer", loggedUser);

            if (loggedUser.getClass().getSimpleName().equals("Admin")) {
                model.addAttribute("transactions", transactionService.getTransactionsByPage(pageID, total));
                int totalPages = calculateTotalPages(total,loggedUser);
                model.addAttribute("totalPages", totalPages);
                return "transactions-admin";
            }
            model.addAttribute("customerTransactions", transactionService
                    .getCustomersTransactionsByPage(pageID, total, loggedUser.getUsername()));
            int totalPages = (transactionService.viewUserTransactions(loggedUser.getUsername()).size()) / total;
            model.addAttribute("totalPages", totalPages);
            return "transactions-customer";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @ModelAttribute("sortOptions")
    public SortTransactionOptions[] populateSortOptions() {
        return SortTransactionOptions.values();
    }

    @ModelAttribute("transactionTypes")
    public TransactionTypeEnum[] populateTransactionType() {
        return TransactionTypeEnum.values();
    }

    private int calculateTotalPages(int numberOfResults, User loggedUser){
        return (transactionService.getAll(loggedUser).size()) / numberOfResults;
    }
}
