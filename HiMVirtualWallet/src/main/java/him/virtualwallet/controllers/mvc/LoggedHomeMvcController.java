package him.virtualwallet.controllers.mvc;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.AuthenticationFailureException;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/logged")
public class LoggedHomeMvcController {

    private UserService userService;
    private final AuthenticationHelper authenticationHelper;

    public LoggedHomeMvcController(UserService userService,
                                   AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showHomePage(HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

////////////////

        if (user.getClass() == Customer.class) {
            return "index-customer";
        }

        else if (user.getClass() == Admin.class) {
            return "index-admin";
        }

        else { return "index"; }
    }

//    @ModelAttribute("customersCount")
//    public String showNumberOfCustomers(Model model) {
//
///////// this will include admins?
//
//        User authenticatedUser = authenticationHelper.tryGetUser(session);
//
//        model.addAttribute("customers", userService.getAll(authenticatedUser));
//        //model.addAttribute("customerSearchDto", new CustomerSearchDto());
//
//        return "index-customer";
//
//    }

}
