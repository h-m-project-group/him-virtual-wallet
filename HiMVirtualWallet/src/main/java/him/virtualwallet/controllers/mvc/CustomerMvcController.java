package him.virtualwallet.controllers.mvc;

import him.virtualwallet.controllers.AuthenticationHelper;
import him.virtualwallet.exceptions.*;
import him.virtualwallet.mappers.UserMapper;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.models.dtos.CustomerDto;
import him.virtualwallet.models.dtos.CustomerSearchDto;
import him.virtualwallet.models.dtos.CustomerUpdateMvcDto;
import him.virtualwallet.models.dtos.UserUpdateMvcDto;
import him.virtualwallet.services.contracts.CardService;
import him.virtualwallet.services.contracts.UserService;
import him.virtualwallet.uploadingfiles.storage.StorageService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/users")
public class CustomerMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ApplicationEventPublisher eventPublisher;
    private final CardService cardService;
    private final StorageService storageService;

    public CustomerMvcController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper, ApplicationEventPublisher eventPublisher, CardService cardService, StorageService storageService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.eventPublisher = eventPublisher;
        this.cardService = cardService;
        this.storageService = storageService;
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {

        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            model.addAttribute("users", userService.getAll(user));
            model.addAttribute("customer", userService.getUserInfoByID(user.getUserID(), user));
            model.addAttribute("customerSearchDto", new CustomerSearchDto());
            return "users";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model, HttpSession session) {

        User authenticatedUser;

        try {
            authenticatedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getByID(id, authenticatedUser);
            model.addAttribute("user", user);
            Customer customer = (Customer) userService.getCustomerInfoByID(user.getUserID(), user);
            model.addAttribute("customer", customer);
            return "user";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());

            return "access-denied";
        }
    }

    // for 'My Account' section

    @GetMapping("/customer-account")
    public String showCustomerInfo(Model model, HttpSession session) {
        User authenticatedUser;
        try {
            authenticatedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {

            Customer user = userService.getCustomerInfoByID(authenticatedUser.getUserID(), authenticatedUser);

            model.addAttribute("user", user);
            model.addAttribute("cards", cardService.getAllCustomerCards(user));
            return "customer-account";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/new")
    public String showNewUserPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("customerDto", new CustomerDto());
        return "customer-new";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("customerDto") CustomerDto userMvcDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {

        User authUser;

        try {
            authUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "customer-new";
        }
        try {
            User user = userMapper.fromDto(userMvcDto);
            model.addAttribute("customerDto", userMvcDto);
            userService.create(user, authUser);
            return "redirect:/customers";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_user", e.getMessage());
            return "customer-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());

            return "access-denied";
        }
    }

    @GetMapping("/{id}/update-account")
    public String showUpdateAccountPage(@PathVariable int id, Model model, HttpSession session) {

        User authenticatedUser;

        try {
            authenticatedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getByID(id, authenticatedUser);
            model.addAttribute("userID", id);
            CustomerUpdateMvcDto customerUpdateMvcDto = userMapper.fromUserToCustomerUpdateDto(user);
            model.addAttribute("customerUpdateDto", customerUpdateMvcDto);

            return "customer-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());

            return "access-denied";
        }
    }

    /*
    Customer updates their own account
     */
    @PostMapping("/{id}/update-account")
    public String updateAccount(@PathVariable int id,
                                @Valid @ModelAttribute("customerUpdateDto") CustomerUpdateMvcDto customerUpdateDto,
                                BindingResult errors,
                                Model model,
                                HttpSession session) {

        User authenticatedUser;

        try {
            authenticatedUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("userID", id);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "customer-update";
        }

        try {

            User user = userMapper.fromDto(customerUpdateDto, id);
            userService.update(user, authenticatedUser);

            if (authenticatedUser.getClass().getSimpleName().equals("Customer")) {

                return "redirect:/users/customer-account";

            } else {
                return "redirect:/users";
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_user", e.getMessage());
            return "customer-update";
        }
    }

    /*
       Admin updates user account
        */
    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            model.addAttribute("userID", id);
            model.addAttribute("userUpdateDto", new UserUpdateMvcDto());

            return "user-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());

            return "access-denied";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("userUpdateDto") UserUpdateMvcDto userUpdateDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {

        User authenticatedUser;

        try {
            authenticatedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "user-update";
        }
        try {

            User user = userMapper.fromDto(userUpdateDto, id);
            userService.update(user, authenticatedUser);

            if (authenticatedUser.getClass().getSimpleName().equals("Customer")) {
                return "redirect:/users/customer-account";

            } else {
                return "redirect:/users";
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_user", e.getMessage());
            return "user-update";
        }
    }

    @PostMapping("/search")
    public String searchCustomers(@ModelAttribute("customerSearchDto") CustomerSearchDto customerSearchDto,
                                  BindingResult errors,
                                  Model model,
                                  HttpSession session) {
        User user;

        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "users";
        }
        try {
            var filtered = userService.search(customerSearchDto.getSearchTerm(), user);

            model.addAttribute("users", filtered);

            return "users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }

    }

    @GetMapping("/uploads")
    public String listUploadedFiles(HttpSession session,
                                    Model model) {
        User user = authenticationHelper.tryGetUser(session);
        model.addAttribute("user", user);
        return "customer-account";
    }

    @PostMapping("/uploads")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes,
                                   Model model,
                                   HttpSession session) throws IOException {

        User user = authenticationHelper.tryGetUser(session);

        storageService.store(file, user);
        model.addAttribute("user", user);

        redirectAttributes.addFlashAttribute("message",
                "You have successfully uploaded " + file.getOriginalFilename() + "");

        return "redirect:/users/uploads";
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }


    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {

        return ResponseEntity.notFound().build();
    }

}
