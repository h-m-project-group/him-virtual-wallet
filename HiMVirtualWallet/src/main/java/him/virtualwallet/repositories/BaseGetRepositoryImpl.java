package him.virtualwallet.repositories;

import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.repositories.contracts.BaseGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public abstract class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    private final Class<T> clazz;
    private final SessionFactory sessionFactory;

    public BaseGetRepositoryImpl(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            var query = String.format("from %s", clazz.getSimpleName());
            return session.createQuery(query, clazz).getResultList();
        }
    }

    @Override
    public T getByID(int id) {
        try (Session session = sessionFactory.openSession()) {
            T entity = session.get(clazz, id);
            if (entity == null) {
                throw new EntityNotFoundException(String.format("%s", clazz), id);
            }
            return entity;
        }
    }

    @Override
    public T getByField(String fieldName, T fieldValue) {
        try (Session session = sessionFactory.openSession()) {
            var query = String.format("from %s where %s = :%s", clazz.getSimpleName(), fieldName, fieldName);
            return session.createQuery(query, clazz)
                    .setParameter(fieldName, fieldValue)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(clazz.getSimpleName(), fieldName, (String) fieldValue));
        }
    }

}
