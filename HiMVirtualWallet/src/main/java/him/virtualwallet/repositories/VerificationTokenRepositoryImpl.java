package him.virtualwallet.repositories;

import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VerificationToken;
import him.virtualwallet.repositories.contracts.VerificationTokenRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationTokenRepositoryImpl implements VerificationTokenRepository {
    private final SessionFactory sessionFactory;

    public VerificationTokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public VerificationToken findByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery("from VerificationToken where user =:user",
                    VerificationToken.class);

            if (query == null) {
                throw new EntityNotFoundException(String.format("%s", VerificationToken.class), user.getUserID());
            }
            return query.uniqueResult();
        }

    }

    @Override
    public VerificationToken getByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<VerificationToken> query = session.createQuery("from VerificationToken where token like :token", VerificationToken.class);
            query.setParameter("token", token);
            if (query == null) {
                throw new EntityNotFoundException(String.format("%s", VerificationToken.class), token, token);
            }
            return query.uniqueResult();
        }
    }

    @Override
    public void create(VerificationToken token) {
        try (Session session = sessionFactory.openSession()) {
            session.save(token);
        }
    }
}