package him.virtualwallet.repositories;

import him.virtualwallet.enums.SortTransactionOptions;
import him.virtualwallet.enums.TransactionTypeEnum;
import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.models.Transaction;
import him.virtualwallet.repositories.contracts.TransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Repository
public class TransactionRepositoryImpl extends BaseModifyRepositoryImpl<Transaction> implements TransactionRepository {

    @Autowired
    public TransactionRepositoryImpl(SessionFactory sessionFactory) {
        super(Transaction.class, sessionFactory);
    }

    @Override
    public List<Transaction> filter(Optional<String> date,
                                    Optional<String> sender,
                                    Optional<String> recipient,
                                    Optional<String> transactionType,
                                    Optional<String> sortOption) {
        try (Session session = getSessionFactory().openSession()) {

            LocalDateTime dateTime = null;
            TransactionTypeEnum transactionTypeEnum = null;

            String queryStr = "FROM Transaction WHERE 1=1";

            var queryParams = new ArrayList<String>();
            queryParams.add("WHERE 1=1");

            if (date.isPresent() && !date.equals("")) {
                DateTimeFormatter formatter = DateTimeFormatter
                        .ofPattern("dd-MM-yyyy HH:mm", Locale.ENGLISH);
               dateTime = LocalDateTime.parse(date.get(),formatter);

                        queryStr+="AND  date =: dateTime";

                queryParams.add(" AND date = :dateTime");
            }
            if (sender.isPresent()) {
                queryStr += " AND sender.username LIKE :sender";
                queryParams.add(" AND sender.username LIKE :sender");
            }
            if (recipient.isPresent() && !recipient.get().equals("")) {
                queryStr += " AND recipient.username LIKE :recipient";
                queryParams.add(" AND recipient.username LIKE :recipient");
            }
            if (transactionType.isPresent() && !transactionType.get().equals("-1")) {
                transactionTypeEnum = TransactionTypeEnum
                        .valueOf(transactionType.get());

                queryStr += " AND transactionType LIKE :transType";
                queryParams.add(" AND transactionType LIKE :transType");
            }
            if (sortOption.isPresent() && !sortOption.get().equals("-1")) {
                SortTransactionOptions sortTransactionOptions = SortTransactionOptions
                        .valueOfPreview(sortOption.get());

                queryStr = "FROM Transaction " +
                        String.join("", queryParams) +
                        sortTransactionOptions.getQuery();
            }

            Query<Transaction> query = session.createQuery(queryStr, Transaction.class);
            LocalDateTime finalDateTime = dateTime;
            date.ifPresent(aDate ->
                    query.setParameter("dateTime", finalDateTime));
            sender.ifPresent(aSender ->
                    query.setParameter("sender", sender.get()));
            recipient.ifPresent(aRecipient ->
                    query.setParameter("recipient", recipient.get()));

            TransactionTypeEnum finalTransactionTypeEnum = transactionTypeEnum;

            transactionType.ifPresent(aTransactionType ->
                    query.setParameter("transType", finalTransactionTypeEnum));

            return query.list();
        }
    }

    @Override
    public List<Transaction> viewUserTransactions(String username) {

        try (Session session = getSessionFactory().openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction where sender.username =:username" +
                    " or recipient.username =: username", Transaction.class);
            query.setParameter("username", username);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Transaction", "username", username);
            }
            return query.list();
        }
    }

    @Override
    public List<Transaction> getTransactionsByPage(int pageId,int total) {

        try (Session session = getSessionFactory().openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction order by transactionID asc",
                    Transaction.class);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("No transactions", pageId);
            }
            return query
                    .setFirstResult(calculateOffset(pageId,total))
                    .setMaxResults(total)
                    .list();
        }
    }

    public List<Transaction> getCustomersTransactionsByPage(int pageId, int total,String username){
        try (Session session = getSessionFactory().openSession()) {
            Query<Transaction> query = session.createQuery("from Transaction where sender.username =:username" +
                    " or recipient.username =: username order by transactionID asc", Transaction.class);
            query.setParameter("username", username);
            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Transaction", "username", username);
            }
            return query
                    .setFirstResult(calculateOffset(pageId, total))
                    .setMaxResults(total)
                    .list();
        }
    }

    private int calculateOffset(int pageID, int total){
        return ((total*pageID) - total);
    }

}
