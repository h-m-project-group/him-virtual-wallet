package him.virtualwallet.repositories;

import him.virtualwallet.models.Card;
import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.CardRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardRepositoryImpl extends BaseModifyRepositoryImpl<Card> implements CardRepository {

    @Autowired
    public CardRepositoryImpl(SessionFactory sessionFactory) {
        super(Card.class, sessionFactory);
    }

    @Override
    public List<Card> getAllCustomerCards(User user) {

        int id = user.getUserID();

        try (Session session = getSessionFactory().openSession()) {
            Query<Card> query = session.createQuery(
                    "FROM Card " +
                            "WHERE user.userID = :id", Card.class);
            query.setParameter("id", id);

            return query.list();
        }
    }

    @Override
    public Card getCardByNumber(long cardNumber) {


        try (Session session = getSessionFactory().openSession()) {
            Query<Card> query = session.createQuery(
                    "FROM Card " +
                            "WHERE cardNumber = :number", Card.class);
            query.setParameter("number", cardNumber);

            return query.getSingleResult();
        }
    }

}
