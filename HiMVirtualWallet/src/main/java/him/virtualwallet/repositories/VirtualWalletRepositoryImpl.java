package him.virtualwallet.repositories;

import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.models.VirtualWallet;
import him.virtualwallet.repositories.contracts.VirtualWalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VirtualWalletRepositoryImpl
        extends BaseModifyRepositoryImpl<VirtualWallet>
        implements VirtualWalletRepository {

    @Autowired
    public VirtualWalletRepositoryImpl(SessionFactory sessionFactory) {
        super(VirtualWallet.class, sessionFactory);
    }

    @Override
    public VirtualWallet getByUserID(int userID) {
        try (Session session = getSessionFactory().openSession()) {

            Query<VirtualWallet> query = session.createQuery("from VirtualWallet where user.userID = :userID", VirtualWallet.class);
            query.setParameter("userID", userID);

            if (query.uniqueResult() == null) {
                throw new EntityNotFoundException("Virtual wallet", "user ID", String.valueOf(userID));
            }
            return query.getSingleResult();
        }
    }
}
