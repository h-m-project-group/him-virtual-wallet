package him.virtualwallet.repositories;

import him.virtualwallet.exceptions.EntityNotFoundException;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public List<User> getAll() {
        try (Session session = getSessionFactory().openSession()) {
            return session.createQuery("from User ", User.class).getResultList();
        }
    }

    @Override
    public List<User> filter(Optional<String> username,
                             Optional<String> email,
                             Optional<String> phone) {

        try (Session session = getSessionFactory().openSession()) {
            String queryStr = "FROM User WHERE 1=1";

            var queryParams = new ArrayList<String>();
            queryParams.add("where 1=1");

            if (username.isPresent()) {
                queryStr += " AND username = :username";
                queryParams.add(" and username = :username");
            }

            if (email.isPresent()) {
                queryStr += " AND email = :email";
                queryParams.add(" AND email = :email");

            }
            if (phone.isPresent()) {
                queryStr += " AND phoneNumber LIKE :phone";
                queryParams.add(" AND phoneNumber LIKE :phone");
            }

            Query<User> query = session.createQuery(queryStr, User.class);

            username.ifPresent(aWeight ->
                    query.setParameter("username", username.get()));
            email.ifPresent(category ->
                    query.setParameter("email", email.get()));
            phone.ifPresent(warehouse ->
                    query.setParameter("phone", phone.get()));

            return query.list();
        }
    }

    @Override
    public List<User> search(String searchTerm) {

        try (Session session = getSessionFactory().openSession()) {

            Query<User> query = session.createQuery(
                    "from User " +
                            "where username =:searchTerm or phoneNumber like :searchTerm " +
                            "or email like :searchTerm", User.class);
            query.setParameter("searchTerm", "%" + searchTerm + "%");

            return query.list();
        }
    }

    @Override
    public User getByUserName(String username) {
        try (Session session = getSessionFactory().openSession()) {

            Query<User> query = session.createQuery("from User where username like :username",
                    User.class);
            query.setParameter("username", username);

            if (query.uniqueResult() == null) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return query.getSingleResult();
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = getSessionFactory().openSession()) {

            Query<User> query = session.createQuery("from User where email like :email", User.class);
            query.setParameter("email", email);

            if (query.uniqueResult() == null) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return query.getSingleResult();
        }
    }

    @Override
    public User getByUsernameWithCard(String username) {
        try (Session session = getSessionFactory().openSession()) {

            Query<Customer> query = session.createQuery(" select s from Customer s inner join " +
                            "Card c on s.userID = c.user.userID where c.user.username = :name ",
                    Customer.class);
            query.setParameter("name", username);

            if (query.uniqueResult() == null) {
                throw new EntityNotFoundException("Card holder", "username", username);
            }
            return query.getSingleResult();
        }
    }

    @Override
    public Customer getCustomerByID(int id) {
        try (Session session = getSessionFactory().openSession()) {
            Customer entity = session.get(Customer.class, id);
            if (entity == null) {
                throw new EntityNotFoundException(String.format("%s", Customer.class), id);
            }
            return entity;
        }
    }

    @Override
    public User getUserByID(int userID) {
        try (Session session = getSessionFactory().openSession()) {
            User entity = session.get(User.class, userID);
            if (entity == null) {
                throw new EntityNotFoundException(String.format("%s", User.class), userID);
            }
            return entity;
        }
    }

    @Override
    public Customer getCustomer(String verificationToken){
        try (Session session = getSessionFactory().openSession()) {

            Query<Customer> query = session.createQuery(" select s from Customer s inner join " +
                            "VerificationToken v on s.userID = v.user.userID where v.token = :verificationToken ",
                    Customer.class);
            query.setParameter("verificationToken", verificationToken);

            if (query.uniqueResult() == null) {
                throw new EntityNotFoundException("User", "token", verificationToken);
            }
            return query.getSingleResult();
        }
    }

}
