package him.virtualwallet.repositories.contracts;

import him.virtualwallet.models.User;
import him.virtualwallet.models.VerificationToken;

public interface VerificationTokenRepository {
    VerificationToken getByToken(String token);

    void create(VerificationToken token);
    VerificationToken findByUser(User user);
}
