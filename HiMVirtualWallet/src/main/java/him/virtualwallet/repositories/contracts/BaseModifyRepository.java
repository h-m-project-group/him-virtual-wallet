package him.virtualwallet.repositories.contracts;

public interface BaseModifyRepository<T> extends BaseGetRepository<T> {
    void create(T t);
    void update(T t);
    void delete(int id);
}
