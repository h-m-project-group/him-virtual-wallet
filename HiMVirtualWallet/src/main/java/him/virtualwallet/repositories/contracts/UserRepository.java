package him.virtualwallet.repositories.contracts;

import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VerificationToken;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseModifyRepository<User> {

    User getByUserName(String username);
    User getByEmail(String email);
    User getByUsernameWithCard(String username);
    User getUserByID(int userID);
    Customer getCustomerByID(int id);

    List<User> filter(Optional<String> username,
                      Optional<String> email,
                      Optional<String> phone);

    List<User> search(String searchTerm);
    Customer getCustomer(String verificationToken);




}
