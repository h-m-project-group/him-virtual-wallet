package him.virtualwallet.repositories.contracts;

import java.util.List;

public interface BaseGetRepository<T>{

    List<T> getAll();
    T getByID(int id);
    T getByField(String fieldName,T fieldValue);

}
