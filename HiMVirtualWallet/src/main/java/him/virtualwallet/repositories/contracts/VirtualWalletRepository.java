package him.virtualwallet.repositories.contracts;

import him.virtualwallet.models.VirtualWallet;

public interface VirtualWalletRepository extends BaseModifyRepository<VirtualWallet> {

    VirtualWallet getByUserID(int userID);
}
