package him.virtualwallet.repositories.contracts;

import him.virtualwallet.models.Card;
import him.virtualwallet.models.User;

import java.util.List;

public interface CardRepository extends BaseModifyRepository<Card> {

    List<Card> getAllCustomerCards(User user);
    Card getCardByNumber(long cardNumber);
}
