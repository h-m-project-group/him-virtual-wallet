package him.virtualwallet.repositories.contracts;

import him.virtualwallet.models.Transaction;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends BaseModifyRepository<Transaction> {


    List<Transaction> filter(Optional<String> date,
                             Optional<String> sender,
                             Optional<String> recipient,
                             Optional<String> transactionType,
                             Optional<String> sortOption);

    List<Transaction> viewUserTransactions(String username);

    List<Transaction> getTransactionsByPage(int pageId,int total);

    List<Transaction> getCustomersTransactionsByPage(int pageId, int total,String username);
}
