package him.virtualwallet.mappers;

import him.virtualwallet.models.Card;
import him.virtualwallet.models.User;
import him.virtualwallet.models.dtos.CardDto;
import him.virtualwallet.repositories.contracts.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class CardMapper {

    private final CardRepository cardRepository;
    private final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

    @Autowired
    public CardMapper(CardRepository cardRepository){
        this.cardRepository = cardRepository;
    }

    public Card fromDto(CardDto cardDto, User currentUser) {

        Card card = new Card();
        dtoToObject(cardDto, card, currentUser);
        return card;
    }

    public Card fromDto(CardDto cardDto, int id) {

        Card card = cardRepository.getByID(id);
        dtoToObject(cardDto, card);
        return card;
    }

    private void dtoToObject(CardDto cardDto, Card card) {
        card.setCardType("debit");
        card.setCardNumber(cardDto.getCardNumber());
        card.setCardHolder(cardDto.getCardHolder());
        card.setCheckNumber(cardDto.getCheckNumber());


        String expirationDate = cardDto.getExpirationDate();

        try {
            Date date = df.parse(expirationDate);
            card.setExpirationDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void dtoToObject(CardDto cardDto, Card card, User currentUser) {
        card.setCardType("debit");
        card.setCardNumber(cardDto.getCardNumber());
        card.setCardHolder(cardDto.getCardHolder());
        card.setCheckNumber(cardDto.getCheckNumber());
        card.setUser(currentUser);

        String expirationDate = cardDto.getExpirationDate();

        try {
            Date date = df.parse(expirationDate);
            card.setExpirationDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public CardDto fromCardToDto(Card card) {

        CardDto cardDto = new CardDto();

        String expirationDate = cardDto.getExpirationDate();

        try {
            Date date = df.parse(expirationDate);
            card.setExpirationDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        cardDto.setCardHolder(card.getCardHolder());
        cardDto.setCardNumber(card.getCardNumber());
        cardDto.setCheckNumber(card.getCheckNumber());
        return cardDto;
    }

}
