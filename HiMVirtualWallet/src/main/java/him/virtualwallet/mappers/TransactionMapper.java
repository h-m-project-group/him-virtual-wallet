package him.virtualwallet.mappers;

import him.virtualwallet.enums.TransactionStatusEnum;
import him.virtualwallet.enums.TransactionTypeEnum;
import him.virtualwallet.models.Transaction;
import him.virtualwallet.models.User;
import him.virtualwallet.models.dtos.DepositDto;
import him.virtualwallet.models.dtos.TransactionDisplayDto;
import him.virtualwallet.models.dtos.TransactionDto;
import him.virtualwallet.repositories.contracts.TransactionRepository;
import him.virtualwallet.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TransactionMapper {

    private final TransactionRepository transactionRepository;
    private final UserRepository userRepository;
    private final LocalDateTime now = LocalDateTime.now();

    @Autowired
    public TransactionMapper(TransactionRepository transactionRepository, UserRepository userRepository) {
        this.transactionRepository = transactionRepository;
        this.userRepository = userRepository;
    }

    public Transaction fromDtoWithUser(TransactionDto transactionDto, User currentUser) {
        Transaction transaction = new Transaction();
        dtoToObject(transactionDto, transaction, currentUser);
        return transaction;
    }

    public TransactionDisplayDto fromTransactionToDto(Transaction transaction) {
        TransactionDisplayDto dto = new TransactionDisplayDto();
        dto.setTransactionID(transaction.getTransactionID());
        dto.setTransactionType(transaction.getTransactionType().toString());
        dto.setSender(transaction.getSender().getUsername());
        dto.setRecipient(transaction.getRecipient().getUsername());
        dto.setTransferSum(transaction.getTransferSum());
        dto.setDate(transaction.getDate().toString());
        dto.setTransactionStatus(transaction.getTransactionStatus().toString());

        return dto;
    }

    public Transaction fromDepositDtoWithUser(DepositDto depositDto, User currentUser) {
        Transaction transaction = new Transaction();
        depositDtoToObject(depositDto, transaction, currentUser);
        return transaction;
    }

    private void dtoToObject(TransactionDto transactionDto, Transaction transaction, User user) {

        transaction.setSender(user);
        User recipient = userRepository.getByUserName(transactionDto.getUsername());
        transaction.setRecipient(recipient);

        TransactionTypeEnum transactionTypeEnum = TransactionTypeEnum.OUTGOING;
        transaction.setTransactionType(transactionTypeEnum);
        transaction.setTransactionStatus(TransactionStatusEnum.PROCESSING);
        transaction.setDate(now);
        transaction.setTransferSum(transactionDto.getTransferSum());
    }

    private void depositDtoToObject(DepositDto depositDto, Transaction transaction, User user) {

        transaction.setSender(user);
        transaction.setRecipient(user);
        transaction.setTransactionType(TransactionTypeEnum.INCOMING);
        transaction.setTransactionStatus(TransactionStatusEnum.PROCESSING);

        transaction.setDate(now);
        transaction.setTransferSum(depositDto.getDepositSum());
    }

}
