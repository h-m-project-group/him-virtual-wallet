package him.virtualwallet.mappers;

import him.virtualwallet.enums.UserStatusEnum;
import him.virtualwallet.enums.VerificationTypeEnum;
import him.virtualwallet.models.Admin;
import him.virtualwallet.models.Customer;
import him.virtualwallet.models.User;
import him.virtualwallet.models.dtos.*;
import him.virtualwallet.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class UserMapper {

    private final UserRepository userRepository;

    @Autowired
    public UserMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User fromDto(CustomerDto customerDto) {

        User customer = new Customer();
        dtoToObject(customerDto, customer);
        return customer;
    }

    public Customer fromRegisterDto(RegisterDto registerDto) {

        Customer customer = new Customer();
        registerDtoToCustomer(registerDto, customer);
        return customer;
    }

    public Admin fromRegisterDtoToAdmin(RegisterDto registerDto) {

        Admin admin = new Admin();
        registerDtoToAdmin(registerDto, admin);
        return admin;
    }

    public Customer fromDto(UserUpdateMvcDto userUpdateMvcDto, int id) {

        Customer customer = userRepository.getCustomerByID(id);
        updateDtoToObject(userUpdateMvcDto, customer);
        return customer;
    }

    /*
    When admin updates customer profile
     */
    public Customer fromDto(CustomerDto customerDto, int id) {

        Customer customer = userRepository.getCustomerByID(id);
        updateDtoToObject(customerDto, customer);
        return customer;
    }

    /*
    When customer updates their own profile
     */
    public Customer fromDto(CustomerUpdateMvcDto customerUpdateDto, int id) {

        Customer customer = userRepository.getCustomerByID(id);
        updateDtoToObject(customerUpdateDto, customer);
        return customer;
    }

    private void dtoToObject(CustomerDto customerDto, User customer) {

        customer.setEmail(customerDto.getEmail());
        customer.setPassword(customerDto.getPassword());
        customer.setPhoneNumber(customerDto.getPhoneNumber());

        UserStatusEnum userStatus = UserStatusEnum.valueOf(customerDto.getUserStatus().toUpperCase(Locale.ROOT));

        customer.setUserStatus(userStatus);
        customer.setPhotoUrl(customer.getPhotoUrl());
    }

    /*
    When admin updates customer profile
     */
    private void updateDtoToObject(CustomerDto customerDto, Customer customer) {

        customer.setEmail(customerDto.getEmail());
        customer.setPassword(customerDto.getPassword());
        customer.setPhoneNumber(customerDto.getPhoneNumber());

        UserStatusEnum userStatus = UserStatusEnum.valueOf(customerDto.getUserStatus().toUpperCase(Locale.ROOT));

        customer.setUserStatus(userStatus);
        customer.setPhotoUrl(customer.getPhotoUrl());

        VerificationTypeEnum verificationTypeEnum = VerificationTypeEnum
                .valueOf(customerDto.getVerificationType());
        customer.setVerificationType(verificationTypeEnum);
    }

    /*
    When customer updates their own profile
     */
    private void updateDtoToObject(CustomerUpdateMvcDto customerUpdateDto, Customer customer) {

        customer.setEmail(customerUpdateDto.getEmail());
        customer.setPassword(customerUpdateDto.getPassword());
        customer.setPhoneNumber(customerUpdateDto.getPhoneNumber());
        customer.setPhotoUrl(customer.getPhotoUrl());
    }

    private void updateDtoToObject(UserUpdateMvcDto userUpdateMvcDto, Customer customer) {

        customer.setEmail(userUpdateMvcDto.getEmail());
        customer.setPhoneNumber(userUpdateMvcDto.getPhoneNumber());
        customer.setPhotoUrl(customer.getPhotoUrl());

        UserStatusEnum userStatus = UserStatusEnum.valueOf(userUpdateMvcDto.getUserStatus().toUpperCase(Locale.ROOT));
        customer.setUserStatus(userStatus);
    }

    private void registerDtoToCustomer(RegisterDto registerDto, Customer customer) {

        customer.setUsername(registerDto.getUsername());
        customer.setEmail(registerDto.getEmail());
        customer.setPassword(registerDto.getPassword());
        customer.setPhoneNumber(registerDto.getPhoneNumber());
        customer.setUserStatus(UserStatusEnum.ACTIVE);
        customer.setVerificationType(VerificationTypeEnum.NOT_VERIFIED);
//        customer.setPhotoUrl("https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png");
        customer.setPhotoUrl("https://www.logolynx.com/images/logolynx/e1/e100b3fc2a67a1e91b72f50277746b97.jpeg");
    }

    private void registerDtoToAdmin(RegisterDto registerDto, Admin admin) {

        admin.setUsername(registerDto.getUsername());
        admin.setEmail(registerDto.getEmail());
        admin.setPassword(registerDto.getPassword());
        admin.setPhoneNumber(registerDto.getPhoneNumber());
        admin.setUserStatus(UserStatusEnum.ACTIVE);
//        admin.setPhotoUrl("https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png");
        admin.setPhotoUrl("https://www.logolynx.com/images/logolynx/e1/e100b3fc2a67a1e91b72f50277746b97.jpeg");
    }

    public CustomerUpdateMvcDto fromUserToCustomerUpdateDto(User user) {

        CustomerUpdateMvcDto customerDto = new CustomerUpdateMvcDto();
        customerDto.setEmail(user.getEmail());
        customerDto.setPhoneNumber(user.getPhoneNumber());
        customerDto.setPassword(user.getPassword());
        return customerDto;
    }

    public CustomerDisplayDto fromUserToCustomerDisplayDto(User customer){

        CustomerDisplayDto customerDisplayDto = new CustomerDisplayDto();
        customerDisplayDto.setCustomerID(customer.getUserID());
        customerDisplayDto.setUsername(customer.getUsername());
        customerDisplayDto.setEmail(customer.getEmail());
        customerDisplayDto.setPhoneNumber(customer.getPhoneNumber());

        return customerDisplayDto;
    }

}
