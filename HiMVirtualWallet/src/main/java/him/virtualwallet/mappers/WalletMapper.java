package him.virtualwallet.mappers;

import him.virtualwallet.exceptions.UnauthorizedOperationException;
import him.virtualwallet.models.User;
import him.virtualwallet.models.VirtualWallet;
import him.virtualwallet.models.dtos.DepositDto;
import him.virtualwallet.models.dtos.DisplayWalletDto;
import him.virtualwallet.models.dtos.VirtualWalletDto;
import him.virtualwallet.models.dtos.VirtualWalletUpdateDto;
import him.virtualwallet.repositories.contracts.UserRepository;
import him.virtualwallet.repositories.contracts.VirtualWalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WalletMapper {

    public static final String WALLET_DEPOSIT_ERROR_MESSAGE = "You cannot make a deposit in another user's wallet!";
    public static final String WALLET_WITHDRAW_ERROR_MESSAGE = "You cannot withdraw funds from another user's wallet!";

    private final VirtualWalletRepository walletRepository;
    private final UserRepository userRepository;

    @Autowired
    public WalletMapper(VirtualWalletRepository walletRepository, UserRepository userRepository) {
        this.walletRepository = walletRepository;
        this.userRepository = userRepository;
    }

    public VirtualWallet fromDto(VirtualWalletDto walletDto) {

        VirtualWallet wallet = new VirtualWallet();
        createDtoToObject(walletDto, wallet);
        return wallet;
    }

    public VirtualWallet depositUpdate(VirtualWalletUpdateDto walletDto, int id, User user) {

        VirtualWallet wallet = walletRepository.getByID(id);

        if (wallet.getUser().getUserID() != user.getUserID()) {
            throw new UnauthorizedOperationException(WALLET_DEPOSIT_ERROR_MESSAGE);
        }

        depositUpdateDtoToObject(walletDto, wallet, user);
        return wallet;
    }

    public VirtualWallet withdrawUpdate(VirtualWalletUpdateDto walletDto, int id, User user) {

        VirtualWallet wallet = walletRepository.getByID(id);

        if (wallet.getUser().getUserID() != user.getUserID()) {
            throw new UnauthorizedOperationException(WALLET_WITHDRAW_ERROR_MESSAGE);
        }

        withdrawUpdateDtoToObject(walletDto, wallet, user);
        return wallet;
    }

    private void createDtoToObject(VirtualWalletDto walletDto, VirtualWallet wallet) {
        User user = userRepository.getByID(walletDto.getUserID());
        wallet.setUser(user);
    }

    private void depositUpdateDtoToObject(VirtualWalletUpdateDto walletDto, VirtualWallet wallet, User user) {
        wallet.setUser(user);
        double sumToAdd = walletDto.getAmount();
        wallet.setTotalAmount(wallet.getTotalAmount() + sumToAdd);
    }

    private void withdrawUpdateDtoToObject(VirtualWalletUpdateDto walletDto, VirtualWallet wallet, User user) {
        wallet.setUser(user);
        double sumToWithdraw = walletDto.getAmount();
        wallet.setTotalAmount(wallet.getTotalAmount() - sumToWithdraw);
    }

    public DisplayWalletDto fromWalletToDisplayWalletDto(VirtualWallet wallet) {

        DisplayWalletDto walletDto = new DisplayWalletDto();

        walletDto.setWalletID(wallet.getWalletID());
        walletDto.setUserID(wallet.getUser().getUserID());
        walletDto.setAmount(wallet.getTotalAmount());

        return walletDto;
    }

    // for MVC deposit

    public VirtualWallet makeDeposit(DepositDto depositDto, int id) {
        VirtualWallet wallet = walletRepository.getByID(id);
        depositDtoToObject(depositDto, wallet);
        return wallet;
    }

    private void depositDtoToObject(DepositDto depositDto, VirtualWallet wallet) {
        User user = userRepository.getByID(wallet.getUser().getUserID());
        wallet.setUser(user);
        double sumToAdd = depositDto.getDepositSum();
        wallet.setTotalAmount(wallet.getTotalAmount() + sumToAdd);
    }

}
