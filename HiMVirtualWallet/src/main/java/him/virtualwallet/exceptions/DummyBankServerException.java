package him.virtualwallet.exceptions;

public class DummyBankServerException extends RuntimeException {

    public DummyBankServerException(String message) {
        super(message);
    }
}
