create table users
(
    users_id int auto_increment
        primary key,
    username varchar(30) not null,
    password varchar(30) not null,
    email varchar(30) not null,
    phone_number varchar(30) not null,
    photo_url varchar(500) null,
    verification_type enum('VERIFIED', 'NOT_VERIFIED') not null,
    user_status_type varchar(30) not null,
    user_type varchar(30) not null,
    constraint users_email_uindex
        unique (email),
    constraint users_username_uindex
        unique (username)
);

create table cards
(
    card_id int auto_increment
        primary key,
    card_type varchar(10) not null,
    card_number mediumtext not null,
    expiration_date date not null,
    card_holder varchar(30) not null,
    check_number int(3) not null,
    user_id int not null,
    constraint cards_card_number_uindex
        unique (card_number) using hash,
    constraint cards_users_users_id_fk
        foreign key (user_id) references users (users_id)
);

create table transactions
(
    transaction_id int auto_increment
        primary key,
    sender_id int not null,
    recipient_id int not null,
    transfer_sum double not null,
    transaction_type enum('INCOMING', 'OUTGOING') not null,
    date date not null,
    transaction_status_type enum('PROCESSING', 'REJECTED', 'CONFIRMED') default 'PROCESSING' null,
    constraint transactions_users_sender_fk
        foreign key (sender_id) references users (users_id),
    constraint transactions_users_users_id_fk
        foreign key (recipient_id) references users (users_id)
);

create index users_verifications_verifications_id_fk
    on users (verification_type);

create table verification_tokens
(
    verification_id int auto_increment
        primary key,
    token varchar(50) not null,
    user_id int not null,
    expiry_date date not null,
    constraint verification_tokens_users_users_id_fk
        foreign key (user_id) references users (users_id)
);

create table wallets
(
    wallet_id int auto_increment
        primary key,
    user_id int not null,
    total_amount double not null,
    constraint wallets_users_users_id_fk
        foreign key (user_id) references users (users_id)
            on delete cascade
);

