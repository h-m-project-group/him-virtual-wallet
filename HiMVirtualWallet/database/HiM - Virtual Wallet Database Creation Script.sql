create table cards
(
    card_id int auto_increment
        primary key,
    card_type varchar(10) not null,
    card_number mediumtext not null,
    expiration_date date not null,
    card_holder varchar(30) not null,
    check_number int(3) not null,
    constraint cards_card_number_uindex
        unique (card_number) using hash
);

create table roles
(
    role_id int auto_increment
        primary key,
    role_type varchar(15) not null,
    constraint roles_role_type_uindex
        unique (role_type)
);

create table transaction_statuses
(
    transaction_status_id int auto_increment
        primary key,
    status_type varchar(30) not null,
    constraint transaction_status_status_type_uindex
        unique (status_type)
);

create table transaction_types
(
    transaction_type_id int auto_increment
        primary key,
    transaction_type_name varchar(20) not null,
    constraint transaction_types_transaction_type_name_uindex
        unique (transaction_type_name)
);

create table user_statuses
(
    user_status_id int auto_increment
        primary key,
    user_status_type varchar(40) not null,
    constraint user_statuses_user_status_type_uindex
        unique (user_status_type)
);

create table admins
(
    user_id int auto_increment
        primary key,
    username varchar(30) not null,
    password varchar(30) not null,
    email varchar(50) not null,
    phone_number varchar(50) not null,
    photo_url varchar(500) not null,
    user_status_id int not null,
    constraint admins_user_statuses_user_status_id_fk
        foreign key (user_status_id) references user_statuses (user_status_id)
);

create table verifications
(
    verification_id int auto_increment
        primary key,
    verification_type varchar(20) not null,
    constraint verifications_verification_type_uindex
        unique (verification_type)
);

create table customers
(
    user_id int auto_increment
        primary key,
    username varchar(20) not null,
    password varchar(30) not null,
    email varchar(30) not null,
    phone_number varchar(10) not null,
    card_id int null,
    photo_url varchar(500) null,
    verification_id int not null,
    user_status_id int not null,
    constraint users_email_uindex
        unique (email),
    constraint users_username_uindex
        unique (username),
    constraint users_cards_card_id_fk
        foreign key (card_id) references cards (card_id),
    constraint users_user_statuses_user_status_id_fk
        foreign key (user_status_id) references user_statuses (user_status_id),
    constraint users_verifications_verification_id_fk
        foreign key (verification_id) references verifications (verification_id)
);

create table transactions
(
    transaction_id int auto_increment
        primary key,
    sender_id int not null,
    recipient_id int not null,
    transfer_sum double not null,
    transaction_type_id int not null,
    date date not null,
    transaction_status_id int not null,
    constraint transactions_transaction_statuses_transaction_status_id_fk
        foreign key (transaction_status_id) references transaction_statuses (transaction_status_id),
    constraint transactions_transaction_types_transaction_type_id_fk
        foreign key (transaction_type_id) references transaction_types (transaction_type_id),
    constraint transactions_users_user_id_fk
        foreign key (sender_id) references customers (user_id),
    constraint transactions_users_user_id_fk_2
        foreign key (recipient_id) references customers (user_id)
);

create table wallets
(
    wallet_id int auto_increment
        primary key,
    user_id int not null,
    total_amount double not null,
    constraint wallets_users_user_id_fk
        foreign key (user_id) references customers (user_id)
);

