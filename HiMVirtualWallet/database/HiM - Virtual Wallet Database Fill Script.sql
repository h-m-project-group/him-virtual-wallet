
# insert into verifications(verification_id, verification_type)
# values (1, 'verified'),
#        (2, 'not verified');
#
# select *
# from verifications;

insert into users (users_id, username, password, email, phone_number, photo_url, verification_type,
                   user_status_type, user_type)
values (1, 'bing', 'chAndl3r.', 'bing@smellycat.com', 0898157486,
        'https://debati.bg/wp-content/uploads/2017/08/Chandler-bing.jpg', 'VERIFIED', 1, 'customer'),
       (2, 'hunter', 'hunT#r11', 'thompson@gonzo.com', 0897548762,
        'https://www.biography.com/.image/t_share/MTgwMjk4MzM1NzUwNDY0NjYy/gettyimages-871379730-copy.jpg', 'VERIFIED', 1, 'customer'),
       (3, 'lee', 'l33Ch.ld', 'lee.child@reacher.com', 0897548762,
        'https://upload.wikimedia.org/wikipedia/commons/e/ee/Lee_Child%2C_Bouchercon_2010.jpg', 'VERIFIED', 1, 'customer'),
       (4, 'nelson', 'n#ls0n26', 'n.demille@goldcoast.com', 0893722584,
        'https://upload.wikimedia.org/wikipedia/commons/c/cf/ND_with_Photo_Credit.jpg', 'VERIFIED', 1, 'customer'),
       (5, 'thornton', 'rEym0nd!', 'chandler@thornton.com', 0899547163,
        'https://ca-times.brightspotcdn.com/dims4/default/71c6fb1/2147483647/strip/true/crop/300x396+0+0/resize/840x1109!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F05%2F08%2Fd00ad9e9ededcc61e33d7eac4a02%2Fla-ca-0712-raymond-chandler-045.jpg-20110717',
        'VERIFIED', 1, 'customer'),
       (6, 'connelly', 'coNNe11.', 'connelly@lincoln.com', 0896258761,
        'https://pbs.twimg.com/profile_images/1328780668717789184/sJisT6jl_400x400.jpg', 'VERIFIED', 1, 'customer'),
       (7, 'reacher', 'r3Ach!rr', 'reacher@tripwire.com', 0987583221,
        'https://upload.wikimedia.org/wikipedia/en/thumb/a/a8/JackReacherAlt.jpg/220px-JackReacherAlt.jpg', 'VERIFIED', 1, 'customer'),
       (8, 'corey', 'J0hnCor#y', 'corey@plumisland.com', 0895541799,
        'https://www.gannett-cdn.com/media/USATODAY/USATODAY/2013/02/20/b07-buzz06-demille-3_4.jpg', 'VERIFIED', 1, 'customer'),
       (9, 'bosch', 'H!3ronymus', 'harry@echopark.com', 0899752648,
        'https://www.sbs.com.au/programs/sites/sbs.com.au.programs/files/styles/body_image/public/bosch.jpg?itok=T9CjaScW&mtime=1470010303',
        'VERIFIED', 1, 'customer'),
       (10, 'mickey', 'hallerHall#r66', 'mickey@brassverdict.com', 0897235876,
        'https://upload.wikimedia.org/wikipedia/en/8/88/Matthew_McConaughey_as_Mickey_Haller.jpg', 'VERIFIED', 1, 'customer'),
       (11, 'tommy', '7ommy$helby', 'tommy@shelbycompanylimited.com', 0894861237,
        'https://i2-prod.dailystar.co.uk/incoming/article19572901.ece/ALTERNATES/s1200c/0_TommySmall.jpg', 'VERIFIED', 1, 'customer'),
       (12, 'ray', 'rayRo#an0', 'ray@romano.com', 0898555783,
        'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F6%2F2019%2F11%2Fgettyimages-53015256-2000.jpg',
       'VERIFIED', 1, 'customer'),
       (13, 'marlowe', 'marL0o##,', 'marlowe@fingerman.com', 0899612475,
        'https://miro.medium.com/max/2928/1*Z3JQ7k-DY8_ikUxM0pPDEg.jpeg', 'VERIFIED', 1, 'customer'),
       (14, 'Monika', '1@35Akoi', 'monika@telerik.alpha', 0898457486,
        'https://debati.bg/wp-content/uploads/2017/08/Chandler-bing.jpg','VERIFIED',0, 'admin'),
       (15, 'Hristo', '0K*da#5o', 'hristo@telerik.alpha', 0865457486,
        'https://debati.bg/wp-content/uploads/2017/08/Chandler-bing.jpg','VERIFIED',0, 'admin');

SELECT *
FROM users;

# No need to add verification_tokens fill script?

insert into cards (card_id, card_type, card_number, expiration_date, card_holder, check_number, user_id)
values (1, 'debit', 5541123501961045, '2027-02-17', 'Chandler Bing', 245, 1),
       (2, 'credit', 7327332265476866, '2025-03-18', 'Hunter Thompson', 866, 2),
       (3, 'credit', 5428559696476762, '2029-08-12', 'James Grant', 762, 3),
       (4, 'debit', 6119700069496081, '2029-08-12', 'Nelson DeMille', 181, 4),
       (5, 'credit', 2700566220939998, '2030-01-22', 'Raymond Chandler', 998, 5),
       (6, 'debit', 6332044471606174, '2031-07-29', 'Michael Connelly', 174, 6),
       (7, 'credit', 8888978070295227, '2024-03-21', 'Jack Reacher', 227, 7),
       (8, 'credit', 5143098532610451, '2026-05-14', 'John Correy', 451, 8),
       (9, 'credit', 7672736538098476, '2028-09-03', 'Hieronymus Bosch', 476, 9),
       (10, 'debit', 4081736838221663, '2023-03-13', 'Michael Haller', 663, 10),
       (11, 'credit', 9431182303277155, '2027-11-02', 'Thomas Shelby', 155, 11),
       (12, 'credit', 7398905074880869, '2022-01-20', 'Raymond Barone', 869, 12),
       (13, 'debit', 6795502220424170, '2025-06-05', 'Philip Marlowe', 170, 13);

SELECT *
FROM cards;

# INSERT INTO roles (role_id, role_type)
# values (1, 'customer'),
#        (2, 'admin');
#
# SELECT *
# FROM roles;

# insert into transaction_statuses(transaction_status_id, status_type)
# values (1, 'processing'),
#        (2, 'confirmed'),
#        (3, 'completed'),
#        (4, 'rejected');
#
#
# select *
# from transaction_statuses;
#
# insert into transaction_types(transaction_type_id, transaction_type_name)
# values (1, 'incoming'),
#        (2, 'outgoing');
#
# select *
# from transaction_types;
#
# insert into user_statuses (user_status_id, user_status_type)
# values (1, 'active'),
#        (2, 'blocked');







# insert into customers (user_id, username, password, email, phone_number, card_id, photo_url, verification_id,
#                        user_status_type)
# values (1, 'bing', 'chAndl3r.', 'bing@smellycat.com', 0898157486, 1,
#         'https://debati.bg/wp-content/uploads/2017/08/Chandler-bing.jpg', 1, 1),
#        (2, 'hunter', 'hunT#r11', 'thompson@gonzo.com', 0897548762, 2,
#         'https://www.biography.com/.image/t_share/MTgwMjk4MzM1NzUwNDY0NjYy/gettyimages-871379730-copy.jpg', 1, 1),
#        (3, 'lee', 'l33Ch.ld', 'lee.child@reacher.com', 0897548762, 3,
#         'https://upload.wikimedia.org/wikipedia/commons/e/ee/Lee_Child%2C_Bouchercon_2010.jpg', 1, 1),
#        (4, 'nelson', 'n#ls0n26', 'n.demille@goldcoast.com', 0893722584, 4,
#         'https://upload.wikimedia.org/wikipedia/commons/c/cf/ND_with_Photo_Credit.jpg', 1, 1),
#        (5, 'thornton', 'rEym0nd!', 'chandler@thornton.com', 0899547163, 5,
#         'https://ca-times.brightspotcdn.com/dims4/default/71c6fb1/2147483647/strip/true/crop/300x396+0+0/resize/840x1109!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F05%2F08%2Fd00ad9e9ededcc61e33d7eac4a02%2Fla-ca-0712-raymond-chandler-045.jpg-20110717',
#         2, 1),
#        (6, 'connelly', 'coNNe11.', 'connelly@lincoln.com', 0896258761, 6,
#         'https://pbs.twimg.com/profile_images/1328780668717789184/sJisT6jl_400x400.jpg', 2, 1),
#        (7, 'reacher', 'r3Ach!rr', 'reacher@tripwire.com', 0987583221, 7,
#         'https://upload.wikimedia.org/wikipedia/en/thumb/a/a8/JackReacherAlt.jpg/220px-JackReacherAlt.jpg', 1, 1),
#        (8, 'corey', 'J0hnCor#y', 'corey@plumisland.com', 0895541799, 8,
#         'https://www.gannett-cdn.com/media/USATODAY/USATODAY/2013/02/20/b07-buzz06-demille-3_4.jpg', 1, 1),
#        (9, 'bosch', 'H!3ronymus', 'harry@echopark.com', 0899752648, 9,
#         'https://www.sbs.com.au/programs/sites/sbs.com.au.programs/files/styles/body_image/public/bosch.jpg?itok=T9CjaScW&mtime=1470010303',
#         1, 1),
#        (10, 'mickey', 'hallerHall#r66', 'mickey@brassverdict.com', 0897235876, 10,
#         'https://upload.wikimedia.org/wikipedia/en/8/88/Matthew_McConaughey_as_Mickey_Haller.jpg', 1, 1),
#        (11, 'tommy', '7ommy$helby', 'tommy@shelbycompanylimited.com', 0894861237, 11,
#         'https://i2-prod.dailystar.co.uk/incoming/article19572901.ece/ALTERNATES/s1200c/0_TommySmall.jpg', 2, 1),
#        (12, 'ray', 'rayRo#an0', 'ray@romano.com', 0898555783, 12,
#         'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F6%2F2019%2F11%2Fgettyimages-53015256-2000.jpg',
#         2, 1),
#        (13, 'marlowe', 'marL0o##,', 'marlowe@fingerman.com', 0899612475, 13,
#         'https://miro.medium.com/max/2928/1*Z3JQ7k-DY8_ikUxM0pPDEg.jpeg', 1, 1);
#
# SELECT *
# FROM customers;
#
# insert into admins (user_id, username, password, email, phone_number, photo_url,user_status_id)
# values (1, 'Monika', '1@35Akoi', 'monika@telerik.alpha', 0898457486,
#         'https://debati.bg/wp-content/uploads/2017/08/Chandler-bing.jpg',1),
#        (2, 'Hristo', '0K*da#5o', 'hristo@telerik.alpha', 0865457486,
#         'https://debati.bg/wp-content/uploads/2017/08/Chandler-bing.jpg',1);



insert into wallets(wallet_id, user_id, total_amount)
values (1, 1, 850.00),
       (2, 2, 50.00),
       (3, 3, 148.75),
       (4, 4, 1114.37),
       (5, 5, 736.28),
       (6, 6, 99.99),
       (7, 7, 2125.20),
       (8, 8, 1162.34),
       (9, 9, 666.66),
       (10, 10, 313.13),
       (11, 11, 25.50),
       (12, 12, 74.47),
       (13, 13, 659.62);

insert into transactions (transaction_id, sender_id, recipient_id, transfer_sum, transaction_type, date,
                          transaction_status_type)
values (1, 1, 2, 20.00, 'INCOMING', '2021-09-10', 'CONFIRMED'),
       (2, 2, 1, 20.00, 'OUTGOING', '2021-09-10', 'PROCESSING');


