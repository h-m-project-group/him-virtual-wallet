# HiM Virtual Wallet

##  The best solution for your online transfers

Virtual Wallet is a web application that enables you to continently manage your budget. Every user can send and receive money (user to user) and put money in his Virtual Wallet (bank to app). 

[![N|Solid](https://pbs.twimg.com/profile_images/1138778977131212801/VOMk3V2X_400x400.jpg)]


- Clients are able to login/logout, update their profile, manage their credit/debit card, make transfers to other users, and view the history of their transfers.
- Each client is able to register one credit or debit card, which is used to transfer money into their Virtual Wallet.
- The transfer is done by a separate dummy REST API. It provides a single endpoint for money withdraw from the credit/debit card. It confirms transfers on random basis (it is dummy, right).
- Each transfer goes through confirmation step and allows either confirming it or editing it.
- Clients are also able to view a list of their transactions filtered by period, recipient, and direction (incoming or outgoing) and sort them by amount and date.
- Admin users are able to see list of all users and search them by phone number, username or email and block or unblock them. Blocked users are not able to make transactions.




## Features

- Supports Pagination
- Filtering and sorting by multiple criteria.
- Image upload to update user profile.
- User verification through email. 
- Multiple credit/debit cards support.

## Tech
The following technologies were used to build this project:

- Java
- Spring Boot, Spring MVC
- Hibernate
- MariaDB
- HTML, CSS
- Thymeleaf
